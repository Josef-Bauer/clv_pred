
allSeqFeatLayerLength <- seqInLength

seqFeatIndVec <- match(embeddingFeatVec, seqFeatVec)

if (length(embeddingFeatVec) > 0 & !useSkipGramEmbeddings) {

    embeddingConfigDT <- data.table(
        seq_feat            = embeddingFeatVec,
        seq_feat_ind        = seqFeatIndVec,
        input_dim           = sapply(1:length(embeddingFeatVec), function(ind) {max(X_DT[[embeddingFeatVec[ind]]]) + 1}),
        output_dim          = embedDimVec,
        input_length        = rep(allSeqFeatLayerLength, length(embeddingFeatVec))
    )

} else {

    embeddingConfigDT <- data.table(NULL)

}


if (useSkipGramEmbeddings) {

    if (file.exists(paste0(path, skipGramEmbeddingFilename))) {

        skipGramEmbeddingMatrix <- readRDS(paste0(path, skipGramEmbeddingFilename))
    
    }

    embedDimVec <- rep(ncol(skipGramEmbeddingMatrix), length(embeddingFeatVec))

    seqFeatIndVec <- match(embeddingFeatVec, seqFeatVec)

    skipGramNumWords <- nrow(skipGramEmbeddingMatrix)

    embeddingConfigDT <- data.table(
        seq_feat            = embeddingFeatVec,
        seq_feat_ind        = seqFeatIndVec,
        input_dim           = skipGramNumWords,
        output_dim          = embedDimVec,
        input_length        = rep(allSeqFeatLayerLength, length(embeddingFeatVec))
    )

}


seqFeatIndVec <- match(convFeatVec, seqFeatVec)

if (length(convFeatVec) > 0) {

    convConfigDT <- data.table(
        seq_feat            = convFeatVec,
        seq_feat_ind        = seqFeatIndVec,
        filters             = filterVec,
        kernel_size         = kernelSizeVec,
        padding             = rep(convPaddingType, length(convFeatVec))
    )

} else {

    convConfigDT <- data.table(NULL)

}

if (length(convFeatVec) > 0) {

    convConfigLayer2DT <- data.table(
        seq_feat            = convFeatVec,
        seq_feat_ind        = seqFeatIndVec,
        filters             = filterLayer2Vec,
        kernel_size         = kernelSizeLayer2Vec,
        padding             = rep(convPaddingType, length(convFeatVec))
    )

} else {

    convConfigLayer2DT <- data.table(NULL)

}



allSeqFeatLayerInputs <- layer_input(shape = c(allSeqFeatLayerLength, numSeqFeat), name = 'allSeqFeatLayerInputs')

sliceSeqFeatNameVec <- rep("", numSeqFeat)

sliceSeqFeatLayerList <- list(NULL)

for (sliceInd in 1:numSeqFeat) {

    sliceSeqFeatNameVec[sliceInd] <- paste0("sliceSeqFeat_", seqFeatVec[sliceInd])

    eval(parse(text = paste0( sliceSeqFeatNameVec[sliceInd], " <- 
                                    allSeqFeatLayerInputs %>% 
                                            layer_lambda(f = function(xVal) {xVal[, , sliceInd]}, name = seqFeatVec[sliceInd])" )))

    sliceSeqFeatLayerList[[sliceInd]] <- get(sliceSeqFeatNameVec[sliceInd])

}

sliceSeqFeatLayerInputList <- sliceSeqFeatLayerList



sliceEmbedFeatNameVec <- rep("", nrow(embeddingConfigDT))

sliceEmbedFeatLayerList <- list(NULL)

if (nrow(embeddingConfigDT) > 0) {

    for (embFeatInd in 1:nrow(embeddingConfigDT)) {

        sliceEmbedFeatNameVec[embFeatInd] <- paste0("sliceEmbedFeat_", embeddingConfigDT[embFeatInd,]$seq_feat)

        eval(parse(text = paste0( sliceEmbedFeatNameVec[embFeatInd], " <- 
                                        sliceSeqFeatLayerList[[embeddingConfigDT[embFeatInd,]$seq_feat_ind]] %>% 
                                                layer_embedding(input_dim       =   embeddingConfigDT[embFeatInd,]$input_dim,
                                                                output_dim      =   embeddingConfigDT[embFeatInd,]$output_dim,
                                                                input_length    =   embeddingConfigDT[embFeatInd,]$input_length,
                                                                name            =   paste0(embeddingConfigDT[embFeatInd,]$seq_feat, '_embed'))
            " )))

        sliceEmbedFeatLayerList[[embeddingConfigDT[embFeatInd,]$seq_feat_ind]] <- get(sliceEmbedFeatNameVec[embFeatInd])

        sliceSeqFeatLayerList[[embeddingConfigDT[embFeatInd,]$seq_feat_ind]] <- get(sliceEmbedFeatNameVec[embFeatInd])

    }

}



for (reshapeInd in setdiff(1:length(seqFeatVec), embeddingConfigDT$seq_feat_ind)) {

    sliceSeqFeatLayerList[[reshapeInd]] <- sliceSeqFeatLayerList[[reshapeInd]] %>% 
                                                    layer_reshape(target_shape = c(dim(sliceSeqFeatLayerList[[reshapeInd]])[2], 1), 
                                                                            name = paste0(seqFeatVec[reshapeInd], '_reshape'))

}



sliceConvFeatNameVec <- rep("", nrow(convConfigDT))

if (nrow(convConfigDT) > 0) {

    for (convFeatInd in 1:nrow(convConfigDT)) {

        sliceConvFeatNameVec[convFeatInd] <- paste0("sliceConvFeat_", convConfigDT[convFeatInd,]$seq_feat)

        eval(parse(text = paste0( sliceConvFeatNameVec[convFeatInd], " <- 
                                        sliceSeqFeatLayerList[[convConfigDT[convFeatInd,]$seq_feat_ind]] %>% 
                                                layer_conv_1d(  filters     =   convConfigDT[convFeatInd,]$filters,
                                                                kernel_size =   convConfigDT[convFeatInd,]$kernel_size,
                                                                padding     =   convConfigDT[convFeatInd,]$padding,
                                                                name        =   paste0(convConfigDT[convFeatInd,]$seq_feat, '_conv'))
            " )))

    if (!useSeparateConv) {

        sliceSeqFeatLayerList[[convConfigDT[convFeatInd,]$seq_feat_ind]] <- get(sliceConvFeatNameVec[convFeatInd])

        }

    }

}


sliceConvLayer2FeatNameVec <- rep("", nrow(convConfigLayer2DT))

if (nrow(convConfigLayer2DT) > 0) {

    for (convFeatInd in 1:nrow(convConfigLayer2DT)) {

        sliceConvLayer2FeatNameVec[convFeatInd] <- paste0("sliceConvLayerFeat_", convConfigLayer2DT[convFeatInd,]$seq_feat)

        if (!useSeparateConv) {

            eval(parse(text = paste0( sliceConvLayer2FeatNameVec[convFeatInd], " <- 
                sliceSeqFeatLayerList[[convConfigLayer2DT[convFeatInd,]$seq_feat_ind]] %>% 
                        layer_conv_1d(  filters     =   convConfigLayer2DT[convFeatInd,]$filters, 
                                        kernel_size =   convConfigLayer2DT[convFeatInd,]$kernel_size,
                                        padding     =   convConfigLayer2DT[convFeatInd,]$padding,
                                        name        =   paste0(convConfigLayer2DT[convFeatInd,]$seq_feat, '_conv_layer2'))
                " )))
                
            sliceSeqFeatLayerList[[convConfigLayer2DT[convFeatInd,]$seq_feat_ind]] <- get(sliceConvLayer2FeatNameVec[convFeatInd])

        } else {

            eval(parse(text = paste0( sliceConvLayer2FeatNameVec[convFeatInd], " <- 
                get(sliceConvFeatNameVec[convFeatInd]) %>% 
                        layer_conv_1d(  filters     =   convConfigLayer2DT[convFeatInd,]$filters,
                                        kernel_size =   convConfigLayer2DT[convFeatInd,]$kernel_size,
                                        padding     =   convConfigLayer2DT[convFeatInd,]$padding,
                                        name        =   paste0(convConfigLayer2DT[convFeatInd,]$seq_feat, '_conv_layer2'))
                " )))
            
        }

    }

}


if (length(seqFeatVec) > 1) {

    if (!useSeparateConv) {

        encoderSeqFeat <- layer_concatenate(inputs = sliceSeqFeatLayerList, axis = 2)

    } else {

        sliceSeqFeatLayerAddConvList <- sliceSeqFeatLayerList

        currLength <- length(sliceSeqFeatLayerAddConvList)

    if (nrow(convConfigDT) > 0) {

        for (convFeatInd in 1:nrow(convConfigDT)) {
        
            sliceSeqFeatLayerAddConvList[[currLength + convFeatInd]] <- get(sliceConvFeatNameVec[convFeatInd])
            
        }
        
    }

    currLength <- length(sliceSeqFeatLayerAddConvList)

    if (nrow(convConfigLayer2DT) > 0) {

        for (convFeatInd in 1:nrow(convConfigLayer2DT)) {
        
            sliceSeqFeatLayerAddConvList[[currLength + convFeatInd]] <- get(sliceConvLayer2FeatNameVec[convFeatInd])
            
        }
            
    }
        
    encoderSeqFeat <- layer_concatenate(inputs = sliceSeqFeatLayerAddConvList, axis = 2)

    }

} else {

    encoderSeqFeat <- sliceSeqFeatLayerList[[1]]

}



encoderGRULayer <- layer_gru(units = nUnitsEncoder, dropout = dropoutEncoder, recurrent_dropout = recurrentDropoutEncoder, 
                                        activation = activationGRU, kernel_initializer = kernelInitGRU, return_state = TRUE)

encoderOutputList <- encoderSeqFeat %>% 
                            encoderGRULayer

hiddenStatesEncoderGRU <- encoderOutputList[[2]]


if (nUnitsEncoder_2 > 0) {

    nUnitsEncoder_2 <- nUnitsEncoder

    hiddenStatesEncoderGRU_1 <- hiddenStatesEncoderGRU
    
    if (useDenseEncoder1ToEncoder2Layer){

        hiddenStatesEncoderGRU_1 <- hiddenStatesEncoderGRU_1 %>% 
                                        layer_dense(units = nUnitsEncoder_2, activation = "tanh")

    }

    encoder_2_outputList <- encoderSeqFeat %>% 
                                layer_gru(units = nUnitsEncoder_2, dropout = dropoutEncoder, recurrent_dropout = recurrentDropoutEncoder, 
                                            activation = activationGRU, kernel_initializer = kernelInitGRU,
                                                return_state = TRUE)(initial_state = hiddenStatesEncoderGRU_1)

    hiddenStatesEncoderGRU_2 <- encoder_2_outputList[[2]]

    hiddenStatesEncoderGRU <- hiddenStatesEncoderGRU_2

}

if (useDenseEncoderToDecoderLayer) {

    hiddenStatesEncoderGRU <- hiddenStatesEncoderGRU %>% 
                                    layer_dense(units = nUnitsDecoder, activation = "tanh")

}


decoderSeqFeat <- encoderSeqFeat

decoderSliceOutput <- layer_lambda(f = function(xVal) {xVal[, (seqInLength - seqOutLength + 1):seqInLength, ]})
 
decoderOutputs <- decoderSeqFeat %>% 
                        layer_gru(units = nUnitsDecoder, dropout = dropoutDecoder, recurrent_dropout = recurrentDropoutDecoder, 
                                    activation = activationGRU, kernel_initializer = kernelInitGRU,
                                        return_sequences = TRUE)(initial_state = hiddenStatesEncoderGRU) %>% 
                                            time_distributed(layer = layer_dense(units = 1)) %>%
                                                decoderSliceOutput

                                
encoderDecoderModel <- keras_model(
    inputs      =   allSeqFeatLayerInputs,
    outputs     =   c(decoderOutputs)
)


if (useSkipGramEmbeddings){

get_layer(encoderDecoderModel, name = "CustomerID_embed") %>% 
    set_weights(list(skipGramEmbeddingMatrix)) %>% 
        freeze_weights()

}


encoderDecoderModel %>% 
    compile(
        optimizer   =   optimizerVal,
        loss        =   lossVal
    )


    
if (!useTrainGen & sampleFracCust == 1 & sampleFracItem == 1)  {

    rm(X_DT, customerFeaturesDT_all, subcategoryNumberOfOrdersSparseMatrix_all, customerOrderTimeDT)
    gc()

}   
    
if (continueTrain) {

    encoderDecoderModel %>% load_model_weights_hdf5(paste0(path, "encoderDecoderModel_full_current_", dataset, ".hdf5"))

}



totalTrueVec <- rowSums(Y_valid_Array[, , 1, drop = FALSE])
finalRmseTotalVec <- numeric(0)
if (use2Valid) {
    totalTrueVec_2 <- rowSums(Y_valid_Array_2[, , 1, drop = FALSE])
    finalRmseTotalVec_2 <- numeric(0)
}

if (useTrainGen) {

    trainHistory <- encoderDecoderModel %>%
        fit_generator(
            generator           =   trainGeneratorSeqCustom(X_DT, batch_size = batchSizeVal), 
            steps_per_epoch     =   1,
            epochs              =   nEpochVal,
            max_queue_size      =   1,
            validation_data     =   list(list(X_valid_Array), Y_valid_Array[, , 1, drop = FALSE]),
            verbose             =   0,
            callbacks           =   list(callback_early_stopping(monitor = "val_loss", patience = earlyStoppingPatienceVal),
                                         callback_model_checkpoint(paste0(path, "best_model_", tryOutIt, "_", runTimestamp, ".h5"), 
                                         save_best_only = TRUE, save_weights_only = TRUE))
        )

    validLossVec <- trainHistory$metrics$val_loss

} else {

    if (repeatSampleFit) {
    
        validLossVec <- numeric(0)
        
        for (epochVal in 1:nEpochVal){
        
            if (epochVal > 1) {
            
            if (sampleFracCust < 1 | sampleFracItem < 1) {
            
                trainGenSourceFun(passToGlobEnv = TRUE)
            
            }
            
            }
            
            trainHistory <- encoderDecoderModel %>%
                fit(
                    x               =   list(X_train_Array),
                    y               =   Y_train_Array[, , 1, drop = FALSE],
                    epochs          =   1,
                    batch_size      =   batchSizeVal,
                    validation_data =   list(list(X_valid_Array), Y_valid_Array[, , 1, drop = FALSE]),
                    verbose         =   0,
                    callbacks       =   list(callback_early_stopping(monitor = "val_loss", patience = earlyStoppingPatienceVal),
                                             callback_model_checkpoint(paste0(path, "best_model_", tryOutIt, "_", runTimestamp, ".h5"), 
                                             save_best_only = TRUE, save_weights_only = TRUE))
                )
            
            validLossVec <- c(validLossVec, trainHistory$metrics$val_loss)
            
            y_valid_pred_Array <- encoderDecoderModel %>% predict(X_valid_Array)
            totalPredVec <- rowSums(y_valid_pred_Array)
            finalRmseTotal <- sqrt(mean((totalPredVec - totalTrueVec)^2))
            finalRmseTotalVec <- c(finalRmseTotalVec, finalRmseTotal)
            
            if (use2Valid) {
                y_valid_pred_Array_2 <- encoderDecoderModel %>% predict(X_valid_Array_2)
                totalPredVec_2 <- rowSums(y_valid_pred_Array_2)
                finalRmseTotal_2 <- sqrt(mean((totalPredVec_2 - totalTrueVec_2)^2))
                finalRmseTotalVec_2 <- c(finalRmseTotalVec_2, finalRmseTotal_2)
            }
            
            if ((lossVal == "mean_squared_error") & (epochVal == 1 | min(finalRmseTotalVec) == finalRmseTotal)) {
            
                save_model_weights_hdf5(encoderDecoderModel, paste0(path, "encoderDecoderModel_currentbest_", dataset, "_", tryOutIt, "_", 
                                                                        runTimestamp, ".hdf5"), overwrite = TRUE)
            
            }
            
            if ((which.min(validLossVec) < length(validLossVec) - earlyStoppingPatienceVal) | 
                (epochVal > stopIfErrorMoreThanPrevFactorDelay & 
                    validLossVec[length(validLossVec)] >= min(validLossVec[-length(validLossVec)]) * stopIfErrorMoreThanPrevFactor)) {
            
                break
            
            }
            
            gc()
        
        }
    
    } else {
    
        trainHistory <- encoderDecoderModel %>% 
            fit(
                x               = list(X_train_Array),
                y               = Y_train_Array[, , 1, drop = FALSE],
                epochs          = nEpochVal,
                batch_size      = batchSizeVal,
                validation_data = list(list(X_valid_Array), Y_valid_Array[, , 1, drop = FALSE]),
                verbose         = 0,
                callbacks       = list(callback_early_stopping(monitor = "val_loss", patience = earlyStoppingPatienceVal),
                                       callback_model_checkpoint(paste0(path, "best_model_", tryOutIt, "_", runTimestamp, ".h5"), 
                                       save_best_only = TRUE, save_weights_only = TRUE))
            )
            
        validLossVec <- trainHistory$metrics$val_loss
    
    }

}


save_model_weights_hdf5(encoderDecoderModel, paste0(path, "encoderDecoderModel_full_", dataset, "_", tryOutIt, "_", runTimestamp, ".hdf5"), overwrite = TRUE)

try(encoderDecoderModel %>% load_model_weights_hdf5(paste0(path, "encoderDecoderModel_currentbest_", dataset, "_", tryOutIt, "_", runTimestamp, ".hdf5")))


y_valid_pred_Array <- encoderDecoderModel %>% 
                            predict(X_valid_Array)

preds_valid_DT <- temp_subslice_X_valid_wide_DT[, iterateVarVec, with = FALSE]

preds_valid_DT <-
    preds_valid_DT[, (paste0("pred_", 1:ncol(y_valid_pred_Array))) := data.table(array_reshape(y_valid_pred_Array, 
                                                                                    c(dim(y_valid_pred_Array)[1], dim(y_valid_pred_Array)[2])))]

true_valid_DT <- temp_subslice_X_valid_wide_DT[, iterateVarVec, with = FALSE]

true_valid_DT <-
    true_valid_DT[, (paste0("true_", 1:ncol(Y_valid_Array[, , 1, drop = FALSE]))) := 
                            data.table(array_reshape(Y_valid_Array[, , 1, drop = FALSE], 
                                            c(dim(Y_valid_Array[, , 1, drop = FALSE])[1], dim(Y_valid_Array[, , 1, drop = FALSE])[2])))]


if (use2Valid) {

    y_valid_pred_Array_2 <- encoderDecoderModel %>% predict(X_valid_Array_2)
    
    preds_valid_DT_2 <- temp_subslice_X_valid_wide_DT_2[, iterateVarVec, with = FALSE]
    
    preds_valid_DT_2 <-
        preds_valid_DT_2[, (paste0("pred_", 1:ncol(y_valid_pred_Array_2))) := 
                            data.table(array_reshape(y_valid_pred_Array_2, c(dim(y_valid_pred_Array_2)[1], dim(y_valid_pred_Array_2)[2])))]
    
    true_valid_DT_2 <- temp_subslice_X_valid_wide_DT_2[, iterateVarVec, with = FALSE]
    
    true_valid_DT_2 <-
        true_valid_DT_2[, (paste0("true_", 1:ncol(Y_valid_Array_2[, , 1, drop = FALSE]))) := 
                            data.table(array_reshape(Y_valid_Array_2[, , 1, drop = FALSE], 
                                                        c(dim(Y_valid_Array_2[, , 1, drop = FALSE])[1], dim(Y_valid_Array_2[, , 1, drop = FALSE])[2])))]

}

try(saveRDS(preds_valid_DT, file = paste0(path, "predsEncDec_valid_DT_", dataset, "_", tryOutIt, ".rds")))

try(saveRDS(true_valid_DT, file = paste0(path, "trueEncDec_valid_DT_", dataset, "_", tryOutIt, ".rds")))

if (use2Valid) {

    try(saveRDS(preds_valid_DT_2, file = paste0(path, "predsEncDec_valid_DT_2_", dataset, "_", tryOutIt, ".rds")))

    try(saveRDS(true_valid_DT_2, file = paste0(path, "trueEncDec_valid_DT_2_", dataset, "_", tryOutIt, ".rds")))

}


finalRmseWeekwise <- sqrt(mean((y_valid_pred_Array - Y_valid_Array[, , 1, drop = FALSE])^2))

finalMseWeekwise <- mean((y_valid_pred_Array - Y_valid_Array[, , 1, drop = FALSE])^2)

totalTrueVec <- rowSums(Y_valid_Array[, , 1, drop = FALSE])

totalPredVec <- rowSums(y_valid_pred_Array)

finalRmseTotal <- sqrt(mean((totalPredVec - totalTrueVec)^2))

if (use2Valid) {

    min(finalRmseTotalVec_2)

}


finalMseTotal <- mean((totalPredVec - totalTrueVec)^2)

finalRmseTotal_2 <- finalRmseTotal

if (use2Valid) {

    totalTrueVec_2 <- rowSums(Y_valid_Array_2[, , 1, drop = FALSE])

    totalPredVec_2 <- rowSums(y_valid_pred_Array_2)

    finalRmseTotal_2 <- sqrt(mean((totalPredVec_2 - totalTrueVec_2)^2))

    finalMseTotal_2 <- mean((totalPredVec_2 - totalTrueVec_2)^2)

}

minValidLossVal <- min(validLossVec)

argminValidLossVal <- which.min(validLossVec)


saveParamResultList <- list(
    dataset                             = dataset,
    runTimestamp                        = runTimestamp,
    tryOutIt                            = tryOutIt,
    continueTrain                       = continueTrain,
    sampleFracCust                      = sampleFracCust,
    sampleFracItem                      = sampleFracItem,
    timeVarName                         = timeVarName,
    minTime                             = minTime,
    maxTime                             = maxTime,
    minValidTime                        = minValidTime,
    maxValidTime                        = maxValidTime,
    minTestTime                         = minTestTime,
    maxTestTime                         = maxTestTime,
    seqInLength                         = seqInLength,
    seqOutLength                        = seqOutLength,
    usePeriodicTrainData                = usePeriodicTrainData,
    useSkipGramEmbeddings               = useSkipGramEmbeddings,
    minRmseTotal                        = min(finalRmseTotalVec),
    minRmseTotal_2                      = min(finalRmseTotalVec_2),
    finalRmseTotalVec                   = paste(as.character(finalRmseTotalVec), collapse = " "),
    finalRmseTotalVec_2                 = paste(as.character(finalRmseTotalVec_2), collapse = " "),
    convPaddingType                     = convPaddingType,
    useSeparateConv                     = useSeparateConv,
    nUnitsEncoder                       = nUnitsEncoder,
    nUnitsEncoder_2                     = nUnitsEncoder_2,
    nUnitsDecoder                       = nUnitsDecoder,
    dropoutEncoder                      = dropoutEncoder,
    recurrentDropoutEncoder             = recurrentDropoutEncoder,
    dropoutDecoder                      = dropoutDecoder,
    recurrentDropoutDecoder             = recurrentDropoutDecoder,
    learnRateVal                        = learnRateVal,
    nEpochVal                           = nEpochVal
)

tryToJSON <- function(xVal) {

    resVal <- try(toJSON(xVal), silent = TRUE)

    if (class(resVal) == "try-error") {
    
        resVal <- toJSON(as.character(xVal))

    }

}

try(saveParamResultJSONList <- lapply(saveParamResultList, tryToJSON), silent=TRUE)


sqlSaveList <- saveParamResultList[sapply(saveParamResultList, is.character) | sapply(saveParamResultList, is.numeric) | 
                                    sapply(saveParamResultList, is.logical)]

collapse <- function(xVal) {

        if (length(xVal) > 1) {

            as.character(tryToJSON(xVal))

        } else {

            xVal

        }

}

sqlSaveList <- lapply(sqlSaveList, collapse)

sqlSaveDT <- as.data.table(sqlSaveList)

con <- dbConnect(SQLite(), dbname = paste0("CLVPredEncDecSaveResultDB_", dataset))

dbWriteTable(con, "saveParamResult", sqlSaveDT, append = TRUE)

dbDisconnect(con)


# ------------------------------------------------
# December 2019
# josef.b.bauer (at) gmail.com

