
numSeqFeat <- length(seqFeatVec)

validationMinTime <- minValidTime

validationMaxTime <- maxValidTime

seqInPlusOutLength <- seqInLength + seqOutLength


uniqueIterateVarList <- list(NULL)

for (ind in 1:length(iterateVarVec)) {

    uniqueIterateVarList[[ind]] <- sort(unique(X_DT[[iterateVarVec[ind]]]))

}


eval(parse(text = paste0("setkey(X_DT, ", iterateVarVec[1], ")[, id := rleid(", iterateVarVec[1], ")]")))

eval(parse(text = paste0("X_DT <- X_DT[, id := rleid(", iterateVarVec[1], ")]")))


trainGenSourceFun <- function(passToGlobEnv = FALSE) {

    custSampleVec <- sort(sample(uniqueIterateVarList[[1]], size = round(sampleFracCust * length(uniqueIterateVarList[[1]])), replace = FALSE))
    
    itemSampleVec <- sort(sample(uniqueIterateVarList[[2]], size = round(sampleFracItem * length(uniqueIterateVarList[[2]])), replace = FALSE))

    for (custInd in 1:length(custSampleVec)) {
        
        custVal <- custSampleVec[custInd]
        
        for (itemInd in 1:length(itemSampleVec)) {
        
            itemVal <- itemSampleVec[itemInd]

            initCond <- custInd == 1 & itemInd == 1

            if (custInd %% 100 == 0) {print(custInd/length(custSampleVec))}

                X_train_sub_DT <- X_DT[J(custVal)][get(timeVarName) <= maxTrainTime & get(timeVarName) >= minTrainTime & get(iterateVarVec[2]) == itemVal]
                
                X_train_sub_DT[[3]] <- pmin(pmax(X_train_sub_DT[[3]], 0), 100)
                
                itIsFirst <- ifelse(initCond, 1, 0)
                
                if (!usePeriodicTrainData) {
                
                    for (seqFeatInd in 1:numSeqFeat) {
                        
                        seqFeatVal <- seqFeatVec[seqFeatInd]

                        temp_subslice_X_train_Array <- t(sapply(1:(length(X_train_sub_DT[[seqFeatVal]]) - seqInPlusOutLength + 1), 
                                                                function(xVal) {X_train_sub_DT[[seqFeatVal]][xVal:(xVal + seqInLength - 1)]}))

                        temp_subslice_Y_train_Array <- t(sapply((seqInPlusOutLength):(length(X_train_sub_DT[[seqFeatVal]])), 
                                                                function(xVal) {X_train_sub_DT[[seqFeatVal]][(xVal - seqOutLength + 1):xVal]}))

                        if (seqFeatInd == 1 & initCond) {
                        
                            currentArrayIndex <- 0

                            X_train_Array <- array(NA, dim = c(dim(temp_subslice_X_train_Array)[1] * length(custSampleVec) * length(itemSampleVec), 
                                                        dim(temp_subslice_X_train_Array)[2], numSeqFeat))
                            
                            Y_train_Array <- array(NA, dim = c(dim(temp_subslice_Y_train_Array)[1] * length(custSampleVec) * length(itemSampleVec), 
                                                        dim(temp_subslice_Y_train_Array)[2], numSeqFeat))

                        }

                        X_train_Array[(currentArrayIndex*(1 - itIsFirst) + 1):(currentArrayIndex*(1 - itIsFirst) + nrow(temp_subslice_X_train_Array)),
                                        , seqFeatInd] <-
                                array_reshape(temp_subslice_X_train_Array, c(dim(temp_subslice_X_train_Array)[1], dim(temp_subslice_X_train_Array)[2], 1))
                                                    
                        Y_train_Array[(currentArrayIndex*(1 - itIsFirst) + 1):(currentArrayIndex*(1 - itIsFirst) + nrow(temp_subslice_Y_train_Array)),
                                        , seqFeatInd] <-
                                array_reshape(temp_subslice_Y_train_Array, c(dim(temp_subslice_Y_train_Array)[1], dim(temp_subslice_Y_train_Array)[2], 1))

                    }
                    
                } else {
                
                    for (seqFeatInd in 1:numSeqFeat) {
                    
                        seqFeatVal <- seqFeatVec[seqFeatInd]
                        
                        temp_subslice_X_train_Array <- t(sapply(rev(seq((length(X_train_sub_DT[[seqFeatVal]]) - seqInPlusOutLength + 1), 1, -periodTrainData)), 
                                                                function(xVal) {X_train_sub_DT[[seqFeatVal]][xVal:(xVal + seqInLength - 1)]}))
                        
                        temp_subslice_Y_train_Array <- t(sapply(rev(seq(length(X_train_sub_DT[[seqFeatVal]]), seqInPlusOutLength, -periodTrainData)), 
                                                                function(xVal) {X_train_sub_DT[[seqFeatVal]][(xVal - seqOutLength + 1):xVal]}))       

                        if (seqFeatInd == 1 & initCond) {
                        
                            currentArrayIndex <- 0
                            
                            X_train_Array <- array(NA, dim = c(dim(temp_subslice_X_train_Array)[1] * length(custSampleVec) * length(itemSampleVec), 
                                                        dim(temp_subslice_X_train_Array)[2], numSeqFeat))
                            
                            Y_train_Array <- array(NA, dim = c(dim(temp_subslice_Y_train_Array)[1] * length(custSampleVec) * length(itemSampleVec), 
                                                        dim(temp_subslice_Y_train_Array)[2], numSeqFeat))
                            
                        }
                        
                        X_train_Array[(currentArrayIndex*(1 - itIsFirst) + 1):(currentArrayIndex*(1 - itIsFirst) + nrow(temp_subslice_X_train_Array)),
                                        , seqFeatInd] <-
                                array_reshape(temp_subslice_X_train_Array, c(dim(temp_subslice_X_train_Array)[1], dim(temp_subslice_X_train_Array)[2], 1))

                        Y_train_Array[(currentArrayIndex*(1 - itIsFirst) + 1):(currentArrayIndex*(1 - itIsFirst) + nrow(temp_subslice_Y_train_Array)),
                                        , seqFeatInd] <-
                                array_reshape(temp_subslice_Y_train_Array, c(dim(temp_subslice_Y_train_Array)[1], dim(temp_subslice_Y_train_Array)[2], 1))
                                                
                    }
                    
                }

                currentArrayIndex <- currentArrayIndex + nrow(temp_subslice_X_train_Array)

        }
    }

    if (passToGlobEnv) {
    
        X_train_Array <<- X_train_Array
        
        Y_train_Array <<- Y_train_Array
        
    }

}


trainGenSourceFun(passToGlobEnv = TRUE)

gc()


trainGeneratorSeqCustom <- function(X_DT, timeVarName = timeVarName, validationMinTime = validationMinTime, uniqueIterateVarList = uniqueIterateVarList, 
                                        seqInLength = seqInLength, seqOutLength = seqOutLength, numSeqFeat = numSeqFeat, 
                                        sampleFracCust = sampleFracCust, sampleFracItem = sampleFracItem, batch_size = -1) {

        function() {

            trainGenSourceFun(passToGlobEnv = TRUE)

            list(X_train_Array, Y_train_Array[, , 1, drop = FALSE])

        }

}
  
  
X_valid_DT <- X_DT[get(timeVarName) < validationMinTime & get(timeVarName) >= validationMinTime - seqInLength]

Y_valid_DT <- X_DT[get(timeVarName) >= validationMinTime & get(timeVarName) <= validationMinTime + seqOutLength - 1]

for (seqFeatInd in 1:numSeqFeat) {

    seqFeatVal <- seqFeatVec[seqFeatInd]

    temp_subslice_X_valid_wide_DT <- eval(parse(text = paste0("data.table::dcast(X_valid_DT, ", iterateVarVec[1], " + ", iterateVarVec[2], " ~ ", 
                                                                    timeVarName, ", value.var = \"", seqFeatVal, "\")")))

    temp_subslice_X_valid_Array <- eval(parse(text = paste0("array_reshape(as.matrix(temp_subslice_X_valid_wide_DT[, 
                                                                    -c(\"", iterateVarVec[1], "\",\"", iterateVarVec[2], "\"), with = FALSE]),  
                                                                                c(nrow(temp_subslice_X_valid_wide_DT), seqInLength, 1))")))

    temp_subslice_Y_valid_wide_DT <- eval(parse(text = paste0("data.table::dcast(Y_valid_DT, ", iterateVarVec[1], " + ", iterateVarVec[2], " ~ ", 
                                                                    timeVarName, ", value.var = \"", seqFeatVal, "\")")))

    temp_subslice_Y_valid_Array <- eval(parse(text = paste0("array_reshape(as.matrix(temp_subslice_Y_valid_wide_DT[, 
                                                                    -c(\"", iterateVarVec[1], "\",\"", iterateVarVec[2], "\"), with = FALSE]),  
                                                                                c(nrow(temp_subslice_Y_valid_wide_DT), seqOutLength, 1))")))

        if (seqFeatInd == 1) {

            X_valid_Array <- array(NA, dim = c(nrow(temp_subslice_X_valid_Array), seqInLength, numSeqFeat))
            
            Y_valid_Array <- array(NA, dim = c(nrow(temp_subslice_Y_valid_Array), seqOutLength, numSeqFeat))

        }

    X_valid_Array[, , seqFeatInd] <- temp_subslice_X_valid_Array

    Y_valid_Array[, , seqFeatInd] <- temp_subslice_Y_valid_Array

}


if (use2Valid) {

    X_valid_DT_2 <- X_DT[get(timeVarName) < validationMinTime + seqOutLength & get(timeVarName) >= validationMinTime - seqInLength + seqOutLength]

    Y_valid_DT_2 <- X_DT[get(timeVarName) >= validationMinTime + seqOutLength & get(timeVarName) <= validationMinTime + 2*seqOutLength - 1]

    for (seqFeatInd in 1:numSeqFeat) {

        seqFeatVal <- seqFeatVec[seqFeatInd]

        temp_subslice_X_valid_wide_DT_2 <- eval(parse(text = paste0("data.table::dcast(X_valid_DT_2, ", iterateVarVec[1], " + ", iterateVarVec[2], " ~ ", 
                                                                        timeVarName, ", value.var = \"", seqFeatVal, "\")")))

        temp_subslice_X_valid_Array_2 <- eval(parse(text = paste0("array_reshape(as.matrix(temp_subslice_X_valid_wide_DT_2[, 
                                                                    -c(\"", iterateVarVec[1], "\",\"", iterateVarVec[2], "\"), with = FALSE]),  
                                                                                    c(nrow(temp_subslice_X_valid_wide_DT_2), seqInLength, 1))")))
                                                                                    
        temp_subslice_Y_valid_wide_DT_2 <- eval(parse(text = paste0("data.table::dcast(Y_valid_DT_2, ", iterateVarVec[1], " + ", iterateVarVec[2], " ~ ", 
                                                                        timeVarName, ", value.var = \"", seqFeatVal, "\")")))

        temp_subslice_Y_valid_Array_2 <- eval(parse(text = paste0("array_reshape(as.matrix(temp_subslice_Y_valid_wide_DT_2[, 
                                                                        -c(\"", iterateVarVec[1], "\",\"", iterateVarVec[2], "\"), with = FALSE]),  
                                                                                    c(nrow(temp_subslice_Y_valid_wide_DT_2), seqOutLength, 1))")))

        if (seqFeatInd == 1) {
        
        X_valid_Array_2 <- array(NA, dim = c(nrow(temp_subslice_X_valid_Array_2), seqInLength, numSeqFeat))
        
        Y_valid_Array_2 <- array(NA, dim = c(nrow(temp_subslice_Y_valid_Array_2), seqOutLength, numSeqFeat))
        
        }
        
        X_valid_Array_2[, , seqFeatInd] <- temp_subslice_X_valid_Array_2
        
        Y_valid_Array_2[, , seqFeatInd] <- temp_subslice_Y_valid_Array_2

    }
  
}


# ------------------------------------------------
# December 2019
# josef.b.bauer (at) gmail.com

