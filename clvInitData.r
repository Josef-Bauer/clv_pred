
if (dataset == "UKRetail") {

    if (!file.exists(paste0(path, "customerOrderDT_", dataset, ".rds"))) {

        if (!file.exists(paste0(path, "Online Retail.xlsx"))) {
        
            download.file(url = "http://archive.ics.uci.edu/ml/machine-learning-databases/00352/Online%20Retail.xlsx",
                            destfile = paste0(path, "Online Retail.xlsx"))
        
        }

        customerOrderDT <- read_excel(paste0(path, "Online Retail.xlsx"))

        setDT(customerOrderDT)

        setnames(customerOrderDT, c("OrderID", "ItemID", "ItemName", "ItemQuantity", "OrderDate", "ItemPrice", "CustomerID", "CustomerCountry"))

        setcolorder(customerOrderDT, c("CustomerID", "OrderID", "OrderDate", "ItemID", "ItemName", "ItemQuantity", "ItemPrice", "CustomerCountry"))

        customerOrderDT[, ItemProfit := ItemQuantity * ItemPrice]

        customerOrderDT[, OrderDate := as.Date(OrderDate)]

        customerOrderDT[, CustomerID := as.integer(as.factor(CustomerID))]
        customerOrderDT[, ItemID := as.integer(as.factor(ItemID))]
        customerOrderDT[, OrderID := as.integer(as.factor(OrderID))]
        customerOrderDT[, CustomerCountry := as.integer(as.factor(CustomerCountry))]

        customerOrderDT[, ItemSubcategory := paste(word(ItemName, 1:2), collapse = " "), by = c("CustomerID", "OrderID", "OrderDate", "ItemID")]
        customerOrderDT[, ItemSubcategory := as.integer(as.factor(ItemSubcategory))]

        customerOrderDT <- customerOrderDT[!is.na(CustomerID)]

        customerTotalProfitAggDT <- customerOrderDT[, list(totalProfit = sum(ItemProfit)), by = "CustomerID"]

        excludeCustomerVec <- customerTotalProfitAggDT[totalProfit >= quantile(totalProfit, 0.8) | 
                                                            totalProfit <= quantile(totalProfit, 0.01)]$CustomerID

        customerOrderDT <- customerOrderDT[ItemProfit <= quantile(ItemProfit, 0.8) & 
                                                ItemProfit >= quantile(ItemProfit, 0.01) & !(CustomerID %in% excludeCustomerVec)]

        allDatesDT <- data.table(OrderDate = seq(min(customerOrderDT$OrderDate), max(customerOrderDT$OrderDate), 1))
        allDatesDT[, WeekID := as.numeric(floor((OrderDate - min(OrderDate))/7))]
        allDatesDT[, DayID := as.numeric(floor(OrderDate - min(OrderDate)))]

        customerOrderDT <- merge(customerOrderDT, allDatesDT, by = "OrderDate", all.x = TRUE, all.y = TRUE, sort = FALSE)

        customerOrderDT[is.na(CustomerID), CustomerID := 0]
        customerOrderDT[is.na(OrderID), OrderID := 0]
        customerOrderDT[is.na(ItemID), ItemID := 0]
        customerOrderDT[is.na(ItemName), ItemName := "NA"]
        customerOrderDT[is.na(ItemQuantity), ItemQuantity := 0]
        customerOrderDT[is.na(ItemPrice), ItemPrice := 0]
        customerOrderDT[is.na(CustomerCountry), CustomerCountry := 0]
        customerOrderDT[is.na(ItemProfit), ItemProfit := 0]
        customerOrderDT[is.na(ItemSubcategory), ItemSubcategory := 0]

        try(saveRDS(customerOrderDT, file = paste0(path, "customerOrderDT_", dataset, ".rds")))

    }

}


# ------------------------------------------------
# December 2019
# josef.b.bauer (at) gmail.com

