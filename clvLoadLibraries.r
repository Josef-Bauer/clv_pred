
# -- Use specified installation repository and install all required libraries if they are not yet installed -- 

options(repos = c(CRAN = "https://cran.revolutionanalytics.com"))

allLibrariesVec <- c(
"data.table",
"Matrix",
"keras",
"tensorflow",
"xgboost",
"reticulate",
"h2o",
"rpart",
"TTR",
"boot",
"zoo",
"hashmap",
"KernSmooth",
"RSQLite",
"jsonlite",
"purrr",
"readxl",
"stringr",
"Ckmeans.1d.dp",
"dplyr"
)


# -- Libraries not yet installed --
newLibrariesVec <- allLibrariesVec[!(allLibrariesVec %in% installed.packages()[,"Package"])]

# # -- If the following is uncommented, all libraries will be freshly installed with their latest versions in the first run 
# #    before the data file is downloaded. --
# if (!file.exists(paste0(path, "Online Retail.xlsx"))) {

    # newLibrariesVec <- allLibrariesVec

# }


if (length(newLibrariesVec) > 0) {

    try(install.packages(newLibrariesVec))

}


# -- Commands for installing a local Python version with TensorFlow within the R directory,
#    should not be needed if Anaconda with TF is already installed on the OS and has an entry in the system path, as it is automatically detected --
# install_tensorflow(version = "gpu")
# install_keras()


# -- Load required libraries --

tryRequire <- function (x) {try(require(x, character.only = TRUE))}

lapply(allLibrariesVec, tryRequire)


# -- Check if GPU computation is working --

library("tensorflow")

with(tf$device("/gpu:0"), {
  try(const <- tf$constant(2))
})

# sess <- tf$Session()
# sess$run(const)

# -- Print Python version in use, to recognize if it is pointing to the right installation --
reticulate::py_config()


# ------------------------------------------------
# December 2019
# josef.b.bauer (at) gmail.com

