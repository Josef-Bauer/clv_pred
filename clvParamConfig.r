
# -- Configuration File for the Encoder-Decoder Sequence-To-Sequence Deep Learning Model for the Customer Lifetime Value Prediction -- 


# -- Name of the dataset to be used, as defined in clvInitData.r -- 
dataset <- "UKRetail"


# -- Specify the time variable determining whether daily data or weekly aggregates should be used as the time level 
#    (or any other defined timeVarName) to model input and output sequences.
#    The remaining variables in this block are used for specifying the splits of the training, validation and test data. -- 

#   - For time series based on daily data:
timeVarName <- "DayID"
minTime <- 0
maxTime <- 258

#   - Define the target time length, which is the number of future time points we want to predict, determining validation and test data size:
targetTimeLength <- 4*7 # 4 weeks as 28 days
minValidTime <- maxTime - 2*targetTimeLength + 1
maxValidTime <- maxTime - targetTimeLength
minTestTime <- maxTime - targetTimeLength + 1
maxTestTime <- maxTime

#   - Alternatively, one can specify the validation and test times explicitly and derive targetTimeLength accordingly:
# targetTimeLength <- (maxValidTime - minValidTime + 1)

#   - The input sequence length, this determines the size of the past time window that is considered by the encoder,
#     e.g., as a multiple of the prediction horizon:
seqInLength <- 2*targetTimeLength
#   - The output sequence length (of the decoder), can be kept as is, identical to the target time length:
seqOutLength <- targetTimeLength
minTrainTime <- 35
maxTrainTime <- minValidTime - 1


# -- Sampling options, for handling huge training data to save computation time and memory demand --

#  - Option for incremental training, computed network weights will be stored locally and loaded for the next fold of training data to avoid cold start:
continueTrain <- FALSE
seedVal <- 1
setSeedCode <- "try(use_session_with_seed(seedVal), silent = TRUE); try(tf$random$set_seed(seedVal), silent = TRUE)"
eval(parse(text = setSeedCode))

#  - Use values < 1 if only a randomly sampled fraction of customers or items should be considered at each epoch (but all can appear in different epochs):
sampleFracCust <- 1
sampleFracItem <- 1
use2Valid <- TRUE

# -- Instead of using all past data points as starting points for the input sequence, only periodic steps can be used for training data generation: --
usePeriodicTrainData <- FALSE
periodTrainData <- 7

excludeCustomersNotPresentInTrain <- TRUE


# -- Specification of the features to be used --

#  - Default sequential features: The total profit (sumProfit - this is also the target to be predicted), the number of unique items, 
#    the time since last and first purchase, the cumulative number of purchases, each of them aggregated by the customer 
#    and the time variable in consideration (e.g., days or weeks). The Customer ID is used for embeddings.
#    Some of these features can be excluded or handled differently, e.g., by convoluting them, as described below.
seqFeatVec <- c("sumProfit", "numberOfUniqueItems", "timeSinceLastPurchase", "timeSinceFirstPurchase", "cumSumPurchases", "CustomerID")

#  - If useAddTrainFeat is TRUE, a larger set of engineered features can be used. In particular, all features defined in clvFeatEng.r, 
#    calculated for every point in time.
#     If specifyExclAddFeat is FALSE, then a positive list of features, as specified in featureAddNames, is included. All features in clvFeatEng.r can be used.
#     If specifyExclAddFeat is TRUE, then a negative list of features can be specified in featureExclNames instead and featureAddNames will be ignored.
#      In particular, if featureExclNames = "", then all features engineered in clvFeatEng.r will be used as additional features in the encoder-decoder model.
useAddTrainFeat <- TRUE

#  - Examples of important features, which are likely well performing in other application scenarios as well. The whole list of features can be found in clvFeatEng.r
featureAddNames <- c("totalNumberOfItems", "totalProfit", "firstRecentGapBetweenOrders", "meanOrderProfit", "minItemPrice", "diffMaxMinOrderProfit", 
        "varRelativeNumberOfRepurchases", "totalNumberOfRecords", "daysSinceLastOrder", "meanItemPrice", "CustomerCountry")
specifyExclAddFeat <- FALSE
featureExclNames <- ""


# -- Settings for embedding features --

#  - If this option is true, then the embedding method proposed in the paper "Customer Lifetime Value Prediction Using Embeddings" by Chamberlain et al. will be used,
#    which is an adaption of the SkipGram model with negative sampling used for text data in NLP (Word2vec) to customer view or in our case customer purchase sequences:
useSkipGramEmbeddings <- TRUE

#  - The following settings are for training a new SkipGram customer embedding model:
skipGramWindowSize <- 11
skipGramNegativeSamples <- 1
skipGramStepsPerEpoch <- 1
skipGramEpochs <- 10000
skipGramEmbeddingSize <- 64

#  - This setting is for using the last precomputed embedding from cache instead of generating a new one for building every new encoder-decoder model:
useSkipGramEmbeddingCache <- TRUE
skipGramEmbeddingFilename <- paste0("skipGramEmbeddingMatrix_", dataset, "_train.rds")
skipGramEmbeddingLocation <- ""
if (useSkipGramEmbeddings) {
    embeddingFeatVec <- c("CustomerID") # not to be changed
    embedDimVec <- rep(skipGramEmbeddingSize, length(embeddingFeatVec)) # not to be changed
    seqFeatVec <- c(setdiff(seqFeatVec, c("CustomerID")), embeddingFeatVec) # not to be changed
} else {
    embeddingFeatVec <- character(0)
    embedDimVec <- rep(skipGramEmbeddingSize, length(embeddingFeatVec))
}

# #  - This part is for using standard embeddings, as available through embedding layers in Keras. Here you can specify which features should be passed 
# #    through an embedding, and the embedding dimension can be prescribed for each feature individually. 
# #    embeddingFeatVec = character(0) means that no embeddings are used.
# #    Uncomment this block and set useSkipGramEmbeddings to FALSE above if you want to use standard embeddings instead of the customer embedding method.
# embeddingFeatVec <- c("CustomerID")
# embedDimVec <- rep(5, length(embeddingFeatVec))
# seqFeatVec <- c(setdiff(seqFeatVec, c("CustomerID")), embeddingFeatVec) # not to be changed


# -- Settings for convolutional layers, which in our case can be thought of as automated smoothing methods --

#  - Specify which sequential features should be passed through a chain of two convolutional layers.
#   - If convFeatVec = setdiff(seqFeatVec, c("CustomerID")), all sequential features are convoluted.
#   - If convFeatVec = "sumProfit", only the sequence of profit data will be passed through the convolutional layers.
convFeatVec <- setdiff(seqFeatVec, c("CustomerID"))
#  - The filters are the number of smoothing patterns to be learned.
#  - The kernel size corresponds to the size of the sliding time window considering neighboring points for smoothing.
#  - Both quantities can be specified for each feature and both layers separately.
filterVec <- rep(2, length(convFeatVec))
kernelSizeVec <- rep(4, length(convFeatVec))
filterLayer2Vec <- rep(2, length(convFeatVec))
kernelSizeLayer2Vec <- rep(3, length(convFeatVec))
#  - If useSeparateConv is TRUE, the original feature sequences will be used in addition to their convolutions, otherwise the convolutions are used instead:
useSeparateConv <- FALSE
#  - This specifies the embedding type, "causal" represents causal temporal convolutions.
convPaddingType <- "causal"


# -- RNN Architecture settings, in particular the number of hidden units in the first and second encoder GRU layer, as well as in the decoder GRU layer,
#    dropout and connectivity settings as well as options for the activation functions and kernel initializations. --

nUnitsEncoder <- 50
nUnitsEncoder_2 <- 50
nUnitsDecoder <- nUnitsEncoder_2
dropoutEncoder <- 0.2
recurrentDropoutEncoder <- 0
dropoutDecoder <- 0.2
recurrentDropoutDecoder <- 0
useDenseEncoder1ToEncoder2Layer <- TRUE
useDenseEncoderToDecoderLayer <- TRUE
activationGRU <- "tanh"
kernelInitGRU <- "glorot_uniform"


# -- Training settings, including options for different customized sampling and early stopping strategies --

learnRateVal <- 0.0001
optimizerVal <- optimizer_adam(lr = learnRateVal)
lossVal <- "mean_squared_error"
nEpochVal <- 50
batchSizeVal <- 16
earlyStoppingPatienceVal <- 6
stopIfErrorMoreThanPrevFactor <- 10
stopIfErrorMoreThanPrevFactorDelay <- 5
useTrainGen <- FALSE
repeatSampleFit <- TRUE


# -- Definition of the experiments, each one corresponding to a tryOutIt index. Every experiment consists of a case block and it is easiest to define 
#    them as deviations with respect to the default parameters defined above.
#    In this case, some of the chosen standard parameter configurations are overwritten. --

if (exists("tryOutIt")) {

    if (tryOutIt == 1) {

        seedVal <- 1
        eval(parse(text = setSeedCode))
        batchSizeVal <- 16
        nUnitsEncoder <- 50
        nUnitsEncoder_2 <- 50
        nUnitsDecoder <- nUnitsEncoder
        optimizerVal <- optimizer_adam(lr = learnRateVal)
        convPaddingType <- "causal"
        useSkipGramEmbeddings <- TRUE
        useSkipGramEmbeddingCache <- TRUE
        skipGramEmbeddingFilename <- paste0("skipGramEmbeddingMatrix_", dataset, "_train.rds")
        skipGramEmbeddingLocation <- ""
        if (useSkipGramEmbeddings) {
            embeddingFeatVec <- c("CustomerID") # not to be changed
            embedDimVec <- rep(skipGramEmbeddingSize, length(embeddingFeatVec)) # not to be changed
            seqFeatVec <- c(setdiff(seqFeatVec, c("CustomerID")), embeddingFeatVec) # not to be changed
        }

    }

}





# ------------------------------------------------
# December 2019
# josef.b.bauer (at) gmail.com

