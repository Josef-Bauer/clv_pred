
# -- Specify the working directory, in which the source files are located and in which results and images will be saved --
# path <- "/home/rstudio/"
path <- paste0(getwd(), "/") # use current working directory

# -- Create an output log file for every run, in which the console output will be stored --
savelogfile <- TRUE

# -- Loop over all tryOutIt index values specified in the configuration file clvParamConfig.r, 
#    each corresponding to an experiment with different model definition or training setup.
#    The index is also appended to the name of images of the models and predictions, which are saved at the end of each iteration. --
for (tryOutIt in c(1)) {

    # -- Clear all values from previous iterations in memory --
    rm(list = setdiff(ls(), c("tryOutIt", "path", "savelogfile")))
    gc()

    # -- The timestamp of execution will be recorded among many other configuration parameters in addition to the results, 
    #    and are stored in databases of the form CLVPred(model)SaveResultDB_(dataset), where (model) and (dataset) are the names of the model
    #    and dataset, respectively. --
    startTime <- Sys.time()
    runTimestamp <- gsub(":", "-", toString(startTime))
    runTimestamp <- gsub(" ", "_", runTimestamp)

    if (savelogfile) {
        consolesave <- file(paste0(path, "clv_logfile_", tryOutIt, "_", runTimestamp, ".log"))
        sink(consolesave, append = TRUE)
        sink(consolesave, append = TRUE, type = "message")
    }

    print(paste0("tryOutIt: ", tryOutIt))

    # -- Runs all code automatically in the required execution order. Files can be either stored locally or in a dropbox, 
    #    which makes it more easy to access the same code from different systems.
    #    Dropbox links will remain the same even if the file content changes when new experiments are added. --

    # -- Load all required libraries, including TensorFlow and Keras interfaces. If the libraries are not yet existent,
    #    they will be installed. In case there is an error during installation, it will be printed. --
    try(source(paste0(path, "clvLoadLibraries.r"), echo = TRUE, max.deparse.length = 1000000))

    # -- Fetch all configuration settings and hyperparameter values stored in clvParamConfig.r.
    #    These include the dataset to be used, processing options such as time splits and folds, the name of the features that should be used,
    #    the embedding method, the configuration of the convolution methods, and all other configurable parameters of the encoder-decoder
    #    recurrent neural network model such as layer sizes of all GRU encoder and decoder layers, the loss to be used
    #    and all training specifications such as learning rate, epochs and sampling options etc. 
    #    Default values are prescribed, which are overwritten for every tryOutIt, defining a new experiment. --
    try(source(paste0(path, "clvParamConfig.r"), echo = TRUE, max.deparse.length = 1000000))

    # -- Load the dataset that should be used. Every new dataset can be registered in this file. 
    #    Note that this is the only file which is explicitly dependent on a given dataset, all remaining code is general
    #    and independent of a specific dataset, in order to make the method easily applicable for new datasets.
    #    Any additions are optional and can be accomplished in the same way as illustrated in the code templates.
    #    The code in clvInitData.r consists of reading in the data and basic preprocessing to convert it into the required internal form. 
    #    This is stored as an image after the first execution, which is loaded in successive iterations as long as this file exists.
    #    Alternatively, the dataset consisting of transactions could be preprocessed elsewhere, it is only required that the internal naming convention is kept: 
    #    "CustomerID", "OrderID", "OrderDate", "ItemID", "ItemSubcategory", "ItemProfit" (the total profit by Customer, Item, OrderDate), 
    #    "ItemQuantity", "ItemPrice", "CustomerCountry", "DayID" and/or "WeekID" encoded as successive integers. --
    try(source(paste0(path, "clvInitData.r"), echo = TRUE, max.deparse.length = 1000000))

    # -- The file link for the main feature engineering code (clvFeatEng.r), which is called within the next script (clvReadGenTSDT.r). 
    #    The code in the clvFeatEng.r file consists of the general feature engineering approach described in our paper.
    #    While this yields an extensive set of useful features, they are at the same time derived from basic information which should be
    #    available in almost all application settings involving CLV prediction. Any sort of new features based on information specific
    #    to a given dataset or problem domain can be easily added in this code, using the existing templates.
    #    Note that all other code does not depend on specific features and their names, all kind of features are stored in variable sized objects 
    #    and their particular role or treatment (e.g., whether they are passed through convolutional layers) can be defined in the configuration file.
    #    This way, no other changes in the remaining code for the proposed method have to be made, which allows for an easy use in practice. --
    featEngSourceLocation <- paste0(path, "clvFeatEng.r")

    # -- In this subprogramm, all sequential features will be generated, including aggregations on the specified time level as well as the 
    #    treatment of missing values. It calls clvFeatEng.r and generates all additional engineered features for all times.
    #    These are stored in images of the form customerFeaturesDT_all_(dataset) after their calculation in the first run
    #    and are loaded in successive iterations in order to save computation time. --
    try(source(paste0(path, "clvReadGenTSDT.r"), echo = TRUE, max.deparse.length = 1000000))

    # -- The following code implements the customer embedding method described in the paper "Customer Lifetime Value Prediction Using Embeddings" by Chamberlain et al.
    #    Since their proposed method is a variant of the SkipGram method with negative sampling (aka Word2vec) for document data, transferred to
    #    customer view (and in our case purchase sequence data), some parts of our code are based on Word2vec examples, which we adapted to the sequential data of the form we have. 
    #    The parameters for training the embedding model can also specified in clvParamConfig.r, including an option for using a precomputed cache,
    #    thus avoiding expensive recomputation. --
    if (tryOutIt == 1) {
        useSkipGramEmbeddingCache <- FALSE
    }
    try(source(paste0(path, "clvSkipGramTrain.r"), echo = TRUE, max.deparse.length = 1000000))

    # -- This part will pick up the results from the previous computations and generates training, validation and test arrays in the final form used by 
    #    the model. This can be done incrementally, by successive sampling during training to avoid huge costs in memory. The arrays consist of sequences of all
    #    features in consideration, as they can be specified in the configuration file. For each customer, a set of a variable number of sequences
    #    is generated by using a sliding window approach with varying start and endpoints in the past, which is a measure against overfitting and helps to improve the generalization 
    #    ability of the model by increasing training data. --
    try(source(paste0(path, "clvGenTrainArrays.r"), echo = TRUE, max.deparse.length = 1000000))

    # -- This code generates the final encoder-decoder sequence-to-sequence model, based on the sequential input and target arrays - the generation of which
    #    is performed on the fly by functions created in clvGenTrainArrays.r. The model can be defined by the easily adjustable architecture and hyperparameter 
    #    configurations, which are specified in clvParamConfig.r and used in a code generator that realizes the respective model in this file. 
    #    It also (optionally) makes use of the learned customer embeddings obtained from clvSkipGramTrain.r.
    #    After the architecture building process, the resulting encoder-decoder model is compiled and iteratively trained using the aforementioned training data generator.
    #    Finally, the trained model weights are saved in files, all predictions on validation and test data are made and the corresponding errors are calculated. 
    #    The results are finally saved as images and a local SQLite database for further evaluation. --
    try(source(paste0(path, "clvEncDecSeqModelTrain.r"), echo = TRUE, max.deparse.length = 1000000))

    # -- This subprogram executes the gradient boosting machine model and computes the results. Currently, xgboost is used as the GBM method.
    #    The previously computed features are passed to the GBM model as well. The customer embeddings are also passed as features vectors, 
    #    where the embedding size determines the number of additional features. More details are described in the paper. --
    try(source(paste0(path, "clvGBMModelTrain.r"), echo = TRUE, max.deparse.length = 1000000))

    if (savelogfile) {
        sink()
    }
    
}


# ------------------------------------------------
# December 2019
# josef.b.bauer (at) gmail.com

