
customerOrderDT <- readRDS(paste0(path, "customerOrderDT_", dataset, ".rds"))

setkeyv(customerOrderDT, c("CustomerID", "OrderDate", "OrderID", "ItemID"))

allDatesDT <- data.table(OrderDate = sort(unique(customerOrderDT$OrderDate)))

if (timeVarName == "WeekID") {

    allDatesDT <- allDatesDT[, (timeVarName) := as.numeric(floor((OrderDate - min(OrderDate))/7))]

}

if (timeVarName == "DayID") {

    allDatesDT <- allDatesDT[, (timeVarName) := as.numeric(floor((OrderDate - min(OrderDate))))]

}

if (!(timeVarName %in% colnames(customerOrderDT))) {

    customerOrderDT <- merge(customerOrderDT, allDatesDT, by = "OrderDate", all.x = TRUE, all.y = FALSE, sort = FALSE)

}

setorder(customerOrderDT, CustomerID, OrderDate, ItemID)

customerOrderDayDT <- customerOrderDT[, list(sumProfit = sum(ItemProfit), numberOfUniqueItems = length(unique(ItemID))), by = c("CustomerID", "OrderDate")]

customerOrderDayDT <- merge(customerOrderDayDT, allDatesDT, by = "OrderDate", all.x = TRUE, all.y = FALSE, sort = FALSE)

customerOrderDayDT <- customerOrderDayDT[, timeID := get(timeVarName)]

customerOrderTimeDT <- customerOrderDayDT[, list(sumProfit = sum(sumProfit), numberOfUniqueItems = sum(numberOfUniqueItems)), by = c("CustomerID", "timeID")]


allCustTimeDT <- CJ(CustomerID = sort(unique(customerOrderTimeDT$CustomerID)), timeID = sort(unique(customerOrderTimeDT$timeID)))

customerOrderTimeDT <- merge(customerOrderTimeDT, allCustTimeDT, by = c("CustomerID", "timeID"), all.x = TRUE, all.y = TRUE, sort = FALSE)

customerOrderTimeDT <- customerOrderTimeDT[is.na(sumProfit), sumProfit := 0]

customerOrderTimeDT <- customerOrderTimeDT[is.na(numberOfUniqueItems), numberOfUniqueItems := 0]

setkeyv(customerOrderTimeDT, c("CustomerID", "timeID"))


baseVal <- ifelse(nrow(allDatesDT) < 10000, 10000, 10 * nrow(allDatesDT))

customerOrderTimeDT <- customerOrderTimeDT[, 
    `:=`(
        timeSinceLastPurchase       = timeID - cummax(ifelse(sumProfit != 0, timeID, -baseVal)),
        timeSinceFirstPurchase      = timeID - cummin(ifelse(sumProfit != 0, timeID, baseVal)),
        cumSumPurchases             = cumsum(sumProfit != 0)
    ), by = "CustomerID"]

customerOrderTimeDT <-
    customerOrderTimeDT[, timeSinceFirstPurchase := ifelse(timeSinceFirstPurchase < 0, 2*baseVal + timeSinceFirstPurchase, timeSinceFirstPurchase)]


X_DT <- customerOrderTimeDT

X_DT <- X_DT[, ItemID := 0]

X_DT <- X_DT[, numberOfUniqueItems := as.numeric(numberOfUniqueItems)]

idCols <- c("CustomerID")

gc()


if (useAddTrainFeat) {

    if (!(file.exists(paste0(path, "customerFeaturesDT_all_", dataset, "_", timeVarName, ".rds")) & 
            file.exists(paste0(path, "customerTargetDT_all_", dataset, "_", timeVarName, ".rds")) & 
            file.exists(paste0(path, "subcategoryNumberOfOrdersSparseMatrix_all_", dataset, "_", timeVarName, ".rds")))) {

        for (shiftVal in 0:max(customerOrderDT[[timeVarName]])) {

            subSet <- paste0("dataShiftAll_", shiftVal)

            try(source(featEngSourceLocation, echo = FALSE))

        }

        gc()

    } else {

        customerFeaturesDT_all <- try(readRDS(file = paste0(path,"customerFeaturesDT_all_", dataset, "_", timeVarName, ".rds")))
        customerTargetDT_all <- try(readRDS(file = paste0(path,"customerTargetDT_all_", dataset, "_", timeVarName, ".rds")))
        subcategoryNumberOfOrdersSparseMatrix_all <- try(readRDS(file = paste0(path,"subcategoryNumberOfOrdersSparseMatrix_all_", 
                                                                                    dataset, "_", timeVarName, ".rds")))

    }
    
    featuresToScale <- c("numberOfOrders", "totalProfit", "meanOrderProfit", "totalNumberOfItems", "totalNumberOfRecords")

    setnames(customerFeaturesDT_all, timeVarName, "timeID")

    scaleF <- function(xVal, timeID) {xVal * targetTimeLength / pmax((timeID - minTrainTime + 1), targetTimeLength)}
    
    for (feat in featuresToScale) {
    
        customerFeaturesDT_all[[feat]] <- as.numeric(customerFeaturesDT_all[[feat]])
        
    }
    
    for (feat in featuresToScale) {
    
        customerFeaturesDT_all <- customerFeaturesDT_all[, (feat) := scaleF(get(feat), timeID), by = c("CustomerID")]
        
    }        

    setnames(customerFeaturesDT_all, "timeID", timeVarName)

    if (!specifyExclAddFeat) {

        featureAddDT <- customerFeaturesDT_all[, c("CustomerID", timeVarName, featureAddNames), with = FALSE]
        
        if ("numberOfUniqueItems" %in% colnames(featureAddDT)) {

            try(setnames(featureAddDT, "numberOfUniqueItems", "sumNumberOfUniqueItems"))

        }

    }
    
    if (specifyExclAddFeat) {

        featureAddDT <- customerFeaturesDT_all[, c(setdiff(colnames(customerFeaturesDT_all), 
                                                    c(featureExclNames, "numberOfUniqueItems", "ItemID", "RowID", "id"))), with = FALSE]

    }

    setnames(featureAddDT, timeVarName, "timeID")

    X_DT <- merge(X_DT, featureAddDT, by = c("CustomerID", "timeID"), all.x = TRUE, all.y = TRUE, sort = FALSE)


    custPastFeatDT <- customerTargetDT_all[, c("CustomerID", "CustomerTotalProfit", timeVarName), with = FALSE]
    
    setnames(custPastFeatDT, timeVarName, "timeID")
    
    custPastFeatDT <- custPastFeatDT[, timeID := timeID + targetTimeLength]
    
    setkeyv(custPastFeatDT, c("CustomerID", "timeID"))
    
    custPastFeatDT <- custPastFeatDT[timeID <= maxTrainTime]
    
    custPastFeatDT <- custPastFeatDT[, CustomerAverageProfitLastTimeLength := CustomerTotalProfit/targetTimeLength]

    custPastFeatDT <- custPastFeatDT[, CustomerAverageProfitPastHistory := cumsum(CustomerAverageProfitLastTimeLength)/(1:.N), by = "CustomerID"]        

    X_DT <- merge(X_DT, custPastFeatDT[, c("CustomerID", "timeID", "CustomerAverageProfitLastTimeLength", "CustomerAverageProfitPastHistory"), with = FALSE], 
                            by = c("CustomerID", "timeID"), all.x = TRUE, all.y = FALSE, sort = FALSE)

    rm(custPastFeatDT)

    X_DT <- X_DT[is.na(CustomerAverageProfitLastTimeLength), CustomerAverageProfitLastTimeLength := 0]
    X_DT <- X_DT[is.na(CustomerAverageProfitPastHistory), CustomerAverageProfitPastHistory := 0]

    
    setkeyv(X_DT, c("CustomerID", "timeID"))

    setorder(X_DT, CustomerID, timeID)

    for (feat in featureAddNames) {

        X_DT <- X_DT[, (feat) := na.locf(get(feat), na.rm = FALSE), by = "CustomerID"]

    }

}


setnames(X_DT, "timeID", timeVarName)

for (feat in setdiff(colnames(X_DT), idCols)) {

    if (class(X_DT[[feat]]) %in% c("numeric", "integer")) {

        X_DT <- X_DT[is.nan(get(feat)), (feat) := -10000]
        X_DT <- X_DT[is.na(get(feat)), (feat) := -20000]
        X_DT <- X_DT[!is.finite(get(feat)), (feat) := -30000]

    }

}


X_DT <- X_DT[CustomerID != 0]

if (excludeCustomersNotPresentInTrain) {

    exclCustVec <- sort(setdiff(unique(customerOrderDT[get(timeVarName) > maxTrainTime | get(timeVarName) < minTrainTime]$CustomerID), 
                                        unique(customerOrderDT[get(timeVarName) <= maxTrainTime & get(timeVarName) >= minTrainTime]$CustomerID)))

    X_DT <- X_DT[!(CustomerID %in% exclCustVec)]

}


iterateVarVec <- c("CustomerID", "ItemID")

seqFeatVec <- c(setdiff(seqFeatVec, c("CustomerID")), embeddingFeatVec)

seqFeatVec <- c(seqFeatVec, setdiff(colnames(X_DT), c(seqFeatVec, "RowID", "id", timeVarName, "ItemID")))


customerOrderDayDT <- customerOrderDayDT[, timeID := get(timeVarName)]

gc()


# ------------------------------------------------
# December 2019
# josef.b.bauer (at) gmail.com

