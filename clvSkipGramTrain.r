
if (!file.exists(paste0(path, "skipGramEmbeddingMatrix_", dataset, "_", "train", ".rds"))) {

    useSkipGramEmbeddingCache <- FALSE

}

if (!useSkipGramEmbeddingCache) {

    for (dataUpToSubset in c("train", "valid", "test")) {

        customerOrderDT <- readRDS(paste0(path, "customerOrderDT_", dataset, ".rds"))
        
        customerOrderDT <- customerOrderDT[CustomerID != 0]

        setkeyv(customerOrderDT, c("CustomerID", "OrderDate", "OrderID", "ItemID"))

        skipGramNumCust <- max(customerOrderDT$CustomerID)

        if (dataUpToSubset == "train") {

            customerOrderDT <- customerOrderDT[get(timeVarName) >= minTrainTime & get(timeVarName) <= maxTrainTime]

        }

        if (dataUpToSubset == "valid") {

            customerOrderDT <- customerOrderDT[get(timeVarName) >= minTrainTime & get(timeVarName) <= maxValidTime]

        }
        
        if (dataUpToSubset == "test") {

            customerOrderDT <- customerOrderDT[get(timeVarName) >= minTrainTime & get(timeVarName) <= maxTestTime]

        }

        setorder(customerOrderDT, ItemID, OrderDate, CustomerID)

        keyColNames <- c("ItemID", "OrderDate", "CustomerID")

        setcolorder(customerOrderDT, c(keyColNames, setdiff(colnames(customerOrderDT), keyColNames)))


        skipGramItemCustList <- eval(parse(text = paste0("split(customerOrderDT[!is.na(", iterateVarVec[1], "), 
                                                                list(", iterateVarVec[2], ", ", iterateVarVec[1], ")], by = \"", iterateVarVec[2], "\")")))

        skipGramItemCustList <- lapply(skipGramItemCustList, `[[`, 2)

        skipGramItemCustList <- lapply(skipGramItemCustList, as.integer)

        skipGramsGenerator <- function (skipGramItemCustList, windowSize, negativeSamples) {

            function() {

                    genIt <- skipGramItemCustList[sample(1:length(skipGramItemCustList), size = 1)]
                    genIt <- unlist(genIt)
                
                while (length(genIt) <= 1) {
                
                    genIt <- skipGramItemCustList[sample(1:length(skipGramItemCustList), size = 1)]
                    genIt <- unlist(genIt)
                
                }
                
                skipVal <- genIt %>%
                    skipgrams(
                        vocabulary_size = skipGramNumCust, 
                        window_size = skipGramWindowSize, 
                        negative_samples = skipGramNegativeSamples
                    )
                    
                xVal <- transpose(skipVal$couples) %>% map(. %>% unlist %>% as.matrix(ncol = 1))
                yVal <- skipVal$labels %>% as.matrix(ncol = 1)
                
                list(xVal, yVal)
                
            }
          
        }


        skipWindow <- skipGramWindowSize
        negativeSamples <- skipGramNegativeSamples
        numSampled <- 1

        inputTarget <- layer_input(shape = 1)
        inputContext <- layer_input(shape = 1)

        skipGramEmbedding <- layer_embedding(
            input_dim   = skipGramNumCust + 1, 
            output_dim  = skipGramEmbeddingSize, 
            name        = "skipGramEmbedding"
        )

        targetVec <- inputTarget %>% 
                        skipGramEmbedding() %>% 
                            layer_flatten()

        contextVec <- inputContext %>%
                        skipGramEmbedding() %>%
                            layer_flatten()


        dotProdLayer <- layer_dot(list(targetVec, contextVec), axes = 1)

        skipGramOutput <- layer_dense(dotProdLayer, units = 1, activation = "sigmoid")

        skipGramModel <- keras_model(list(inputTarget, inputContext), skipGramOutput)

        skipGramModel %>% compile(loss = "binary_crossentropy", optimizer = "adam")

        skipGramModel %>%
          fit_generator(
                skipGramsGenerator(skipGramItemCustList, skipWindow, negativeSamples), 
                steps_per_epoch = skipGramStepsPerEpoch, 
                epochs = skipGramEpochs,
                verbose = 0
            )

        skipGramEmbeddingMatrix <- get_weights(skipGramModel)[[1]]

        try(saveRDS(skipGramEmbeddingMatrix, file = paste0(path, "skipGramEmbeddingMatrix_", dataset, "_", dataUpToSubset, ".rds")))

    }

}


# ------------------------------------------------
# December 2019
# josef.b.bauer (at) gmail.com

