

customerOrderDT <- readRDS(paste0(path, "customerOrderDT_", dataset, ".rds"))

setkeyv(customerOrderDT, c("CustomerID", "OrderDate", "OrderID", "ItemID"))

setorder(customerOrderDT, CustomerID, OrderDate, ItemID)


if (!exists("gbmTryOutIt")) {

    gbmTryOutIt <- tryOutIt

}

if (gbmTryOutIt == 1) {

    objective_val <- "reg:linear"
    eval_metric_val <- "rmse"
    nround_val <- 500
    eta_val <- 0.05
    max_depth_val <- 8
    subsample_val <- 0.8
    colsample_bytree_val <- 0.8
    early.stop.round_val <- 20
    booster_val <- "gbtree"

}


if (!(file.exists(paste0(path, "customerFeaturesDT_all_", dataset, "_", timeVarName, ".rds")) & 
        file.exists(paste0(path, "customerTargetDT_all_", dataset, "_", timeVarName, ".rds")) & 
        file.exists(paste0(path, "subcategoryNumberOfOrdersSparseMatrix_all_",     dataset, "_", timeVarName, ".rds")))) {

    if (!exists("maxShiftVal")) {

        maxShiftVal <- max(customerOrderDT[[timeVarName]])

    }

    if (maxShiftVal == -1) {

        maxShiftVal <- max(customerOrderDT[[timeVarName]])

    }

    for (shiftVal in 0:maxShiftVal) {

        subSet <- paste0("dataShiftAll_", shiftVal)

        try(source(paste0(path, "clvFeatEng.r"), echo = FALSE))

    }

} else {

    subSet <- "all"

    eval(parse(text = paste0("customerFeaturesDT_", subSet, " <- 
                                try(readRDS(file = paste0(path, \"customerFeaturesDT_all_\",\"", dataset, "_", timeVarName, ".rds\")))")))
    eval(parse(text = paste0("customerTargetDT_", subSet, " <- 
                                try(readRDS(file = paste0(path, \"customerTargetDT_all_\",\"", dataset, "_", timeVarName, ".rds\")))")))
    eval(parse(text = paste0("subcategoryNumberOfOrdersSparseMatrix_", subSet, " <- 
                                try(readRDS(file = paste0(path, \"subcategoryNumberOfOrdersSparseMatrix_all_\",\"", 
                                    dataset, "_", timeVarName, ".rds\")))")))

}


custPastFeatDT <- customerTargetDT_all[, c("CustomerID", "CustomerTotalProfit", timeVarName), with = FALSE]

setnames(custPastFeatDT, timeVarName, "timeID")

custPastFeatDT <- custPastFeatDT[, timeID := timeID + targetTimeLength]

setkeyv(custPastFeatDT, c("CustomerID", "timeID"))

custPastFeatDT <- custPastFeatDT[timeID <= maxTrainTime]

custPastFeatDT <- custPastFeatDT[, CustomerAverageProfitLastTimeLength := CustomerTotalProfit/targetTimeLength]

custPastFeatDT <- custPastFeatDT[, CustomerAverageProfitPastHistory := cumsum(CustomerAverageProfitLastTimeLength)/(1:.N), by = "CustomerID"]

setnames(custPastFeatDT, "timeID", timeVarName)

customerFeaturesDT_all <- merge(customerFeaturesDT_all,
                                    custPastFeatDT[, c("CustomerID", timeVarName, "CustomerAverageProfitLastTimeLength", 
                                                            "CustomerAverageProfitPastHistory"), with = FALSE], 
                                        by = c("CustomerID", timeVarName), all.x = TRUE, all.y = FALSE, sort = FALSE)
                                        
rm(custPastFeatDT)

customerFeaturesDT_all <- customerFeaturesDT_all[is.na(CustomerAverageProfitLastTimeLength), CustomerAverageProfitLastTimeLength := 0]
customerFeaturesDT_all <- customerFeaturesDT_all[is.na(CustomerAverageProfitPastHistory), CustomerAverageProfitPastHistory := 0]


includeFeatInd <- which(!(customerFeaturesDT_all[["CustomerID"]] == 0))

customerFeaturesDT_all <- customerFeaturesDT_all[includeFeatInd, ]
customerTargetDT_all <- customerTargetDT_all[includeFeatInd, ]
subcategoryNumberOfOrdersSparseMatrix_all <- subcategoryNumberOfOrdersSparseMatrix_all[includeFeatInd, ]


if (excludeCustomersNotPresentInTrain) {

    exclCustVec <- sort(setdiff(unique(customerOrderDT[get(timeVarName) > maxTrainTime | get(timeVarName) < minTrainTime]$CustomerID), 
                                    unique(customerOrderDT[get(timeVarName) <= maxTrainTime & get(timeVarName) >= minTrainTime]$CustomerID)))

    includeFeatInd <- which(!(customerFeaturesDT_all[["CustomerID"]] %in% exclCustVec))

    customerFeaturesDT_all <- customerFeaturesDT_all[includeFeatInd, ]
    customerTargetDT_all <- customerTargetDT_all[includeFeatInd, ]
    subcategoryNumberOfOrdersSparseMatrix_all <- subcategoryNumberOfOrdersSparseMatrix_all[includeFeatInd, ]

}


if (file.exists(paste0(path, skipGramEmbeddingFilename))) {

    skipGramEmbeddingMatrix <- readRDS(paste0(path, skipGramEmbeddingFilename))

}

skipGramEmbeddingDT <- as.data.table(skipGramEmbeddingMatrix)

setnames(skipGramEmbeddingDT, paste0("embed_", 1:ncol(skipGramEmbeddingDT)))

skipGramEmbeddingDT <- skipGramEmbeddingDT[, CustomerID := 1:nrow(skipGramEmbeddingDT)]

setcolorder(skipGramEmbeddingDT, c("CustomerID", setdiff(colnames(skipGramEmbeddingDT), "CustomerID")))

customerFeaturesDT_all <- merge(customerFeaturesDT_all, skipGramEmbeddingDT, by = c("CustomerID"), all.x = TRUE, all.y = FALSE, sort = FALSE)


trainTimesVec <- minTrainTime:(maxTrainTime - targetTimeLength)
validTimesVec <- minValidTime - 1
testTimesVec <- minTestTime - 1

trainFeatInd <- which(customerFeaturesDT_all[[timeVarName]] %in% trainTimesVec)
validFeatInd <- which(customerFeaturesDT_all[[timeVarName]] %in% validTimesVec)
testFeatInd <- which(customerFeaturesDT_all[[timeVarName]] %in% testTimesVec)

customerFeaturesDT_train <- customerFeaturesDT_all[get(timeVarName) %in% trainTimesVec]
customerFeaturesDT_valid <- customerFeaturesDT_all[get(timeVarName) %in% validTimesVec]
customerFeaturesDT_test <- customerFeaturesDT_all[get(timeVarName) %in% testTimesVec]

customerTargetDT_train <- customerTargetDT_all[get(timeVarName) %in% trainTimesVec]
customerTargetDT_valid <- customerTargetDT_all[get(timeVarName) %in% validTimesVec]
customerTargetDT_test <- customerTargetDT_all[get(timeVarName) %in% testTimesVec]

subcategoryNumberOfOrdersSparseMatrix_train <- subcategoryNumberOfOrdersSparseMatrix_all[trainFeatInd, ]
subcategoryNumberOfOrdersSparseMatrix_valid <- subcategoryNumberOfOrdersSparseMatrix_all[validFeatInd, ]
subcategoryNumberOfOrdersSparseMatrix_test <- subcategoryNumberOfOrdersSparseMatrix_all[testFeatInd, ]



for (subSet in c("train","valid","test")) {

    eval(parse(text = paste0("subcategoryNumberOfOrdersSparseMatrix_", subSet,
                                " <- subcategoryNumberOfOrdersSparseMatrix_", subSet, "[, colSums(subcategoryNumberOfOrdersSparseMatrix_", subSet, ") > 0]")))

}


customerFeaturesDT_train <- customerFeaturesDT_train[, RowID := 1:nrow(customerFeaturesDT_train)]
customerFeaturesDT_valid <- customerFeaturesDT_valid[, RowID := 1:nrow(customerFeaturesDT_valid)]
customerFeaturesDT_test <- customerFeaturesDT_test[, RowID := 1:nrow(customerFeaturesDT_test)]

customerTargetDT_train <- customerTargetDT_train[, RowID := 1:nrow(customerTargetDT_train)]
customerTargetDT_valid <- customerTargetDT_valid[, RowID := 1:nrow(customerTargetDT_valid)]
customerTargetDT_test <- customerTargetDT_test[, RowID := 1:nrow(customerTargetDT_test)]


excludeIDs <- customerTargetDT_train[abs(CustomerTotalProfit) > 1000000]$RowID
useIDs <- customerFeaturesDT_train$RowID
useIDs <- setdiff(useIDs, excludeIDs)

trainIDs <- which(customerFeaturesDT_train$RowID %in% useIDs)
validIDs <- customerFeaturesDT_valid$RowID
testIDs <- customerFeaturesDT_test$RowID


allFeatureNames <- setdiff(colnames(customerFeaturesDT_train), c("CustomerID", "ItemID", "RowID", timeVarName))

allFeatureNames <- c(allFeatureNames, colnames(subcategoryNumberOfOrdersSparseMatrix_train))


featureNames <- allFeatureNames

if (!exists("includeFeatNames")) {

    includeFeatNames <- ""

}

if (!exists("excludeFeatNames")) {

    excludeFeatNames <- ""

}


includeFeatNames <- c(includeFeatNames, paste0("embed_", 1:ncol(skipGramEmbeddingDT)))
    

if (includeFeatNames[1] != "") {

    featureNames <- intersect(featureNames, c(includeFeatNames, colnames(subcategoryNumberOfOrdersSparseMatrix_train)))

}

if (excludeFeatNames != "") {

    featureNames <- setdiff(featureNames, excludeFeatNames)

}


y_train <- customerTargetDT_train[trainIDs, CustomerTotalProfit]
y_valid <- customerTargetDT_valid[validIDs, CustomerTotalProfit]
y_test <- customerTargetDT_test[testIDs, CustomerTotalProfit]


dtrain <- xgb.DMatrix(data = cbind(data.matrix(customerFeaturesDT_train[trainIDs, intersect(featureNames, colnames(customerFeaturesDT_train)), with=FALSE]), 
            subcategoryNumberOfOrdersSparseMatrix_train[trainIDs, intersect(featureNames, colnames(subcategoryNumberOfOrdersSparseMatrix_train))]), 
            label = y_train, missing=NaN)
            
rm(customerFeaturesDT_train, customerTargetDT_train); gc()

dvalid <- xgb.DMatrix(data = cbind(data.matrix(customerFeaturesDT_valid[validIDs, intersect(featureNames, colnames(customerFeaturesDT_valid)), with=FALSE]), 
            subcategoryNumberOfOrdersSparseMatrix_valid[validIDs, intersect(featureNames, colnames(subcategoryNumberOfOrdersSparseMatrix_valid))]), 
            label = y_valid, missing=NaN)
            
rm(customerFeaturesDT_valid, customerTargetDT_valid); gc()

dtest <- xgb.DMatrix(data = cbind(data.matrix(customerFeaturesDT_test[testIDs, intersect(featureNames, colnames(customerFeaturesDT_test)), with=FALSE]), 
            subcategoryNumberOfOrdersSparseMatrix_test[testIDs, intersect(featureNames, colnames(subcategoryNumberOfOrdersSparseMatrix_test))]),
            label = y_test, missing=NaN)
            
rm(customerFeaturesDT_test, customerTargetDT_test); gc()

param_setting <- list(
                     objective = objective_val,
                     eta = eta_val,
                     max_depth = max_depth_val,
                     eval_metric = eval_metric_val,
                     subsample = subsample_val,
                     colsample_bytree = colsample_bytree_val,
                     booster = booster_val
                   )

if (exists("xgboost_tree_method")) {

    param_setting$tree_method = xgboost_tree_method

}

if (exists("xgboost_grow_policy")) {

    param_setting$grow_policy = xgboost_grow_policy

}


set.seed(seedVal)

xgb_model <- xgb.train(data = dtrain,
                   watchlist = list(train = dtrain, valid = dvalid),
                   params = param_setting,
                   nrounds = nround_val,
                   early_stopping_rounds = early.stop.round_val,
                   verbose = 0,
                   maximize = FALSE
)


try(saveRDS(xgb_model, file = paste0(path, "xgb_model", "_", dataset, "_", gbmTryOutIt, ".rds")))

predicted_y_valid <- predict(xgb_model, dvalid)

try(saveRDS(predicted_y_valid, file = paste0(path, "predicted_y_valid", "_", dataset, "_", gbmTryOutIt, ".rds")))

minRmseTotalValid <- sqrt(mean((y_valid - predicted_y_valid)^2))


predicted_y_test <- predict(xgb_model, dtest)

try(saveRDS(predicted_y_test, file = paste0(path, "predicted_y_test", "_", dataset, "_", gbmTryOutIt, ".rds")))

minRmseTotalTest <- sqrt(mean((y_test - predicted_y_test)^2))

print(minRmseTotalTest)


saveParamResultList <- list(
    dataset                             = dataset,
    runTimestamp                        = runTimestamp,
    gbmTryOutIt                         = gbmTryOutIt,
    timeVarName                         = timeVarName,
    minTime                             = minTime,
    maxTime                             = maxTime,
    minValidTime                        = minValidTime,
    maxValidTime                        = maxValidTime,
    minTestTime                         = minTestTime,
    maxTestTime                         = maxTestTime,
    minRmseTotalValid                   = minRmseTotalValid,
    minRmseTotalTest                    = minRmseTotalTest,
    nround_val                          = nround_val,
    eta_val                             = eta_val,
    max_depth_val                       = max_depth_val,
    subsample_val                       = subsample_val,
    colsample_bytree_val                = colsample_bytree_val,
    early.stop.round_val                = early.stop.round_val,
    booster_val                         = booster_val
)

tryToJSON <- function(xVal) {

    resVal <- try(toJSON(xVal), silent = TRUE)

    if (class(resVal) == "try-error") {
    
        resVal <- toJSON(as.character(xVal))

    }

}

try(saveParamResultJSONList <- lapply(saveParamResultList, tryToJSON), silent=TRUE)


sqlSaveList <- saveParamResultList[sapply(saveParamResultList, is.character) | sapply(saveParamResultList, is.numeric) | 
                                    sapply(saveParamResultList, is.logical)]

collapse <- function(xVal) {

        if (length(xVal) > 1) {

            as.character(tryToJSON(xVal))

        } else {

            xVal

        }

}

sqlSaveList <- lapply(sqlSaveList, collapse)

sqlSaveDT <- as.data.table(sqlSaveList)

con <- dbConnect(SQLite(), dbname = paste0("CLVPredGBMSaveResultDB_", dataset))

dbWriteTable(con, "saveParamResult", sqlSaveDT, append = TRUE)

dbDisconnect(con)


# ------------------------------------------------
# December 2019
# josef.b.bauer (at) gmail.com

