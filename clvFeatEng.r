
print(subSet)

if (subSet == "trainLast") {

    customerOrderDT_subSet <- customerOrderDT[get(timeVarName) < minValidTime - targetTimeLength]

    customerOrderDT_subSet <- customerOrderDT_subSet[, DaysSinceOrder := as.numeric(max(customerOrderDT_subSet$OrderDate) - OrderDate)]

    customerTargetDT_subSet <- customerOrderDT[get(timeVarName) >= minValidTime - targetTimeLength & get(timeVarName) < minValidTime, 
                                                list(CustomerTotalProfit = sum(ItemProfit)), by = "CustomerID"]

}

if (grepl("trainShift", subSet)) {

    customerOrderDT_subSet <- customerOrderDT[get(timeVarName) < minValidTime - targetTimeLength - shiftVal]

    customerOrderDT_subSet <- customerOrderDT_subSet[, DaysSinceOrder := as.numeric(max(customerOrderDT_subSet$OrderDate) - OrderDate)]

    customerTargetDT_subSet <- customerOrderDT[get(timeVarName) >= minValidTime - targetTimeLength - shiftVal & get(timeVarName) < minValidTime - shiftVal, 
                                                    list(CustomerTotalProfit = sum(ItemProfit)), by = "CustomerID"]

}

if (subSet == "validLast") {

    customerOrderDT_subSet <- customerOrderDT[get(timeVarName) < minTestTime - targetTimeLength]

    customerOrderDT_subSet <- customerOrderDT_subSet[, DaysSinceOrder := as.numeric(max(customerOrderDT_subSet$OrderDate) - OrderDate)]

    customerTargetDT_subSet <- customerOrderDT[get(timeVarName) >= minTestTime - targetTimeLength & get(timeVarName) < minTestTime, 
                                                    list(CustomerTotalProfit = sum(ItemProfit)), by = "CustomerID"]

}

if (grepl("validShift", subSet)) {

    customerOrderDT_subSet <- customerOrderDT[get(timeVarName) < minTestTime - targetTimeLength - shiftVal & 
                                                get(timeVarName) >= minValidTime - targetTimeLength]

    customerOrderDT_subSet <- customerOrderDT_subSet[, DaysSinceOrder := as.numeric(max(customerOrderDT_subSet$OrderDate) - OrderDate)]

    customerTargetDT_subSet <- customerOrderDT[get(timeVarName) >= minTestTime - targetTimeLength - shiftVal & get(timeVarName) < minTestTime - shiftVal, 
                                                    list(CustomerTotalProfit = sum(ItemProfit)), by = "CustomerID"]

}

if (subSet == "testLast") {

    customerOrderDT_subSet <- customerOrderDT[get(timeVarName) < minTestTime]

    customerOrderDT_subSet <- customerOrderDT_subSet[, DaysSinceOrder := as.numeric(max(customerOrderDT_subSet$OrderDate) - OrderDate)]

    customerTargetDT_subSet <- customerOrderDT[get(timeVarName) >= minTestTime & get(timeVarName) <= maxTestTime, 
                                                    list(CustomerTotalProfit = sum(ItemProfit)), by = "CustomerID"]

}

if (grepl("testShift", subSet)) {

    customerOrderDT_subSet <- customerOrderDT[get(timeVarName) < minTestTime - shiftVal & get(timeVarName) >= minTestTime - targetTimeLength]

    customerOrderDT_subSet <- customerOrderDT_subSet[, DaysSinceOrder := as.numeric(max(customerOrderDT_subSet$OrderDate) - OrderDate)]

    customerTargetDT_subSet <- customerOrderDT[get(timeVarName) >= minTestTime - shiftVal & get(timeVarName) <= maxTestTime - shiftVal, 
                                                    list(CustomerTotalProfit = sum(ItemProfit)), by = "CustomerID"]

}

if (grepl("dataShiftAll", subSet)) {

    customerOrderDT_subSet <- customerOrderDT[get(timeVarName) <= max(customerOrderDT[[timeVarName]]) - shiftVal]

    customerOrderDT_subSet <- customerOrderDT_subSet[, DaysSinceOrder := as.numeric(max(customerOrderDT_subSet$OrderDate) - OrderDate)]

    customerTargetAllRemainingDT_subSet <- customerOrderDT[get(timeVarName) > max(customerOrderDT[[timeVarName]]) - shiftVal, 
                                                    list(CustomerTotalProfit = sum(ItemProfit)), by = "CustomerID"]

    customerTargetDT_subSet <- customerOrderDT[get(timeVarName) > max(customerOrderDT[[timeVarName]]) - shiftVal & 
                                                get(timeVarName) <= max(customerOrderDT[[timeVarName]]) - shiftVal + targetTimeLength, 
                                                    list(CustomerTotalProfit = sum(ItemProfit)), by = "CustomerID"]

}


maxDate_subSet <- max(customerOrderDT_subSet$OrderDate)

maxTimeVar_subSet <- max(customerOrderDT_subSet[[timeVarName]])

customerOrderDT_subSet <- customerOrderDT_subSet[CustomerID != 0]

setkeyv(customerOrderDT_subSet, c("CustomerID", "OrderDate", "ItemID"))


customerOrderLevelDT_subSet <- customerOrderDT_subSet[, list(
    DaysSinceOrder      = min(DaysSinceOrder),
    OrderProfit         = sum(ItemProfit),
    numberOfOrders      = length(unique(OrderID))
), by = c("CustomerID", "OrderDate")]

setkeyv(customerOrderLevelDT_subSet, c("CustomerID","OrderDate"))

customerOrderDate <- customerOrderDT[, list(
    OrderProfit         = sum(ItemProfit),
    WeekID              = min(WeekID)
), by = c("CustomerID", "OrderDate")]

setorder(customerOrderDate, CustomerID, OrderDate)


customerFeaturesFromOrderLevelDT_subSet <- customerOrderLevelDT_subSet[, list(
    daysSinceLastOrder                  = min(DaysSinceOrder), 
    daysSinceFirstOrder                 = max(DaysSinceOrder), 
    medianDaysSinceOrder                = median(DaysSinceOrder), 
    meanDaysSinceOrder                  = mean(DaysSinceOrder), 
    maxGapBetweenOrders                 = max(diff(sort(DaysSinceOrder)), na.rm = TRUE), 
    meanGapBetweenOrders                = mean(diff(sort(DaysSinceOrder)), na.rm = TRUE), 
    firstRecentGapBetweenOrders         = diff(sort(DaysSinceOrder)[1:2]), 
    secondRecentGapBetweenOrders        = diff(sort(DaysSinceOrder)[2:3]), 
    numberOfOrders                      = sum(numberOfOrders), 
    totalProfit                         = sum(OrderProfit), 
    meanOrderProfit                     = mean(OrderProfit), 
    minOrderProfit                      = min(OrderProfit), 
    maxOrderProfit                      = max(OrderProfit), 
    varDaysSinceOrder                   = var(DaysSinceOrder), 
    varGapBetweenOrders                 = var(diff(sort(DaysSinceOrder))), 
    varOrderProfit                      = var(OrderProfit), 
    meanDiffOrderProfit                 = mean(diff(OrderProfit)), 
    sdOrderDate                         = sd(OrderDate), 
    meanOrderDate                       = mean(as.numeric(OrderDate)),
    numberOfOrdersLast3Months            = sum(numberOfOrders[DaysSinceOrder <= 90])
), by = "CustomerID"]

customerFeaturesFromOrderLevelDT_subSet <-
    customerFeaturesFromOrderLevelDT_subSet[, diffDaysLastFirstOrder            := daysSinceLastOrder - daysSinceFirstOrder]
customerFeaturesFromOrderLevelDT_subSet <-
    customerFeaturesFromOrderLevelDT_subSet[, fracFirstMeanGapBetweenOrders     := firstRecentGapBetweenOrders / meanGapBetweenOrders]
customerFeaturesFromOrderLevelDT_subSet <-
    customerFeaturesFromOrderLevelDT_subSet[, fracDaysLastFirstOrder            := (daysSinceLastOrder - daysSinceFirstOrder) / daysSinceLastOrder]
customerFeaturesFromOrderLevelDT_subSet <-
    customerFeaturesFromOrderLevelDT_subSet[, diffMaxMinOrderProfit             := maxOrderProfit - minOrderProfit]

useLags <- TRUE
if (exists("useLags")) {
if (useLags) {

    customerFeaturesLagsDT_subSet <- customerOrderLevelDT_subSet[, list(
        profitOrderLag_0         = OrderProfit[.N], 
        profitOrderLag_1         = ifelse(length(OrderProfit) - 1 > 0, OrderProfit[.N - 1], 0),
        daysSinceOrderLag_0      = DaysSinceOrder[.N], 
        daysSinceOrderLag_1      = ifelse(length(DaysSinceOrder) - 1 > 0, DaysSinceOrder[.N - 1], 0),
        numberOfOrderLag_0       = numberOfOrders[.N], 
        numberOfOrderLag_1       = ifelse(length(numberOfOrders) - 1 > 0, numberOfOrders[.N - 1], as.integer(0))
    ), by = "CustomerID"]

    customerFeaturesFromOrderLevelDT_subSet <- merge(customerFeaturesFromOrderLevelDT_subSet, customerFeaturesLagsDT_subSet, 
                                                            by = "CustomerID", all.x = TRUE, all.y = TRUE, sort = FALSE)

}
}

itemPurchaseDT_subSet <- customerOrderDT_subSet[, list(
    ItemQuantity    = sum(ItemQuantity), 
    OrderID         = min(OrderID)
), by = c("CustomerID", "OrderDate", "ItemID")]

itemPurchaseDT_subSet <-
    itemPurchaseDT_subSet[, 
        minOrderIDByCustItem    := min(OrderID), 
       by = c("CustomerID", "ItemID")]

itemPurchaseDT_subSet <-
    itemPurchaseDT_subSet[, isFirstOrder    := ifelse(OrderID == minOrderIDByCustItem, 1, 0)]


itemNumberOfRepurchasesDT_subSet <- itemPurchaseDT_subSet[, list(
    numberOfRepurchasesByCustItem   = length(unique(OrderID[isFirstOrder == 0]))
), by = c("CustomerID", "ItemID")]

itemNumberOfRepurchasesDT_subSet <- itemNumberOfRepurchasesDT_subSet[, list(
    meanNumberOfRepurchases = mean(numberOfRepurchasesByCustItem)
), by = c("ItemID")]


setkey(itemPurchaseDT_subSet, "ItemID")

itemPurchaseStatsDT_subSet <- itemPurchaseDT_subSet[, list(
    numberOfOrdersItem              = length(unique(OrderID)), 
    numberOfCustomersItem           = length(unique(CustomerID)), 
    absoluteNumberOfRepurchases     = length(unique(OrderID[isFirstOrder == 0])),
    numberOfCustomersReorderedItem  = length(unique(CustomerID[isFirstOrder == 0]))
), by = "ItemID"]

itemPurchaseStatsDT_subSet <-
    itemPurchaseStatsDT_subSet[, c("relativeNumberOfRepurchases", "relativeNumberOfCustomersReorderedItem") 
                                := list(absoluteNumberOfRepurchases / numberOfOrdersItem, numberOfCustomersReorderedItem / numberOfCustomersItem)]

itemPurchaseStatsDT_subSet <- merge(itemPurchaseStatsDT_subSet, itemNumberOfRepurchasesDT_subSet, by = "ItemID", all.x = TRUE, all.y = TRUE)


custItemMinOrderDT <- customerOrderDT_subSet[, list(
    minOrderIDByCustItem    = min(OrderID), 
    minOrderItemQuantity    = sum(ItemQuantity[OrderID == min(OrderID)])
), by = c("CustomerID", "ItemID")]


itemSuccessorDT <- merge(custItemMinOrderDT, 
                            itemPurchaseDT_subSet[isFirstOrder == 0, -c("minOrderIDByCustItem"), with = FALSE][, -c("isFirstOrder"), with = FALSE], 
                                by = "CustomerID", all.x = TRUE, all.y = FALSE, sort = FALSE, allow.cartesian = TRUE)
    
itemSuccessorDT <- itemSuccessorDT[, minOrderIDByCustItem  := as.integer(minOrderIDByCustItem)]
itemSuccessorDT <- itemSuccessorDT[, OrderID               := as.integer(OrderID)]

itemSuccessorDT <- itemSuccessorDT[OrderID > minOrderIDByCustItem]


itemSuccessorCountDT <- itemSuccessorDT[, list(
    numberOfSuccessiveOrders                = .N, 
    numberOfCustomersSuccessivePurchase     = length(unique(CustomerID))
), by = c("ItemID.x", "ItemID.y")]

itemSuccessorCountDT <- itemSuccessorCountDT[, ItemID := ItemID.y]

itemSuccessorCountDT <- merge(itemSuccessorCountDT, itemPurchaseStatsDT_subSet[, list(ItemID, numberOfOrdersItem, numberOfCustomersItem)], 
                                by = "ItemID", all.x = TRUE, all.y = FALSE, sort = FALSE)

itemSuccessorCountDT <-
    itemSuccessorCountDT[, relativeNumberCustomersSuccessivePurchase := numberOfCustomersSuccessivePurchase/numberOfCustomersItem]

itemSuccessorCountDT <-
    itemSuccessorCountDT[, relativeNumberSuccessiveOrders := numberOfSuccessiveOrders/numberOfOrdersItem]

itemSuccessorCountDT <- itemSuccessorCountDT[ItemID.x != ItemID.y]


customerOrderDT_subSet <- merge(customerOrderDT_subSet, itemPurchaseStatsDT_subSet, by = "ItemID", all.x = TRUE, all.y = FALSE)

customerFeaturesFromItemLevelDT_subSet <- customerOrderDT_subSet[, list(
    numberOfUniqueItems                             = length(unique(ItemID)), 
    numberOfUniqueSubcategories                     = length(unique(ItemSubcategory)), 
    minItemPrice                                    = min(ItemPrice), 
    meanItemPrice                                   = mean(ItemPrice), 
    maxItemPrice                                    = max(ItemPrice), 
    minNumberOfOrdersItem                           = min(numberOfOrdersItem), 
    minNumberOfCustomersItem                        = min(numberOfCustomersItem), 
    minAbsoluteNumberOfRepurchases                  = min(absoluteNumberOfRepurchases), 
    minNumberOfCustomersReorderedItem               = min(numberOfCustomersReorderedItem), 
    minRelativeNumberOfRepurchases                  = min(relativeNumberOfRepurchases), 
    minRelativeNumberOfCustomersReorderedItem       = min(relativeNumberOfCustomersReorderedItem), 
    minMeanNumberOfRepurchases                      = min(meanNumberOfRepurchases), 
    maxNumberOfOrdersItem                           = max(numberOfOrdersItem), 
    maxNumberOfCustomersItem                        = max(numberOfCustomersItem), 
    maxAbsoluteNumberOfRepurchases                  = max(absoluteNumberOfRepurchases), 
    maxNumberOfCustomersReorderedItem               = max(numberOfCustomersReorderedItem), 
    maxRelativeNumberOfRepurchases                  = max(relativeNumberOfRepurchases), 
    maxRelativeNumberOfCustomersReorderedItem       = max(relativeNumberOfCustomersReorderedItem), 
    maxMeanNumberOfRepurchases                      = max(meanNumberOfRepurchases), 
    meanNumberOfOrdersItem                          = mean(numberOfOrdersItem), 
    meanNumberOfCustomersItem                       = mean(numberOfCustomersItem), 
    meanAbsoluteNumberOfRepurchases                 = mean(absoluteNumberOfRepurchases), 
    meanNumberOfCustomersReorderedItem              = mean(numberOfCustomersReorderedItem), 
    meanRelativeNumberOfRepurchases                 = mean(relativeNumberOfRepurchases), 
    meanRelativeNumberOfCustomersReorderedItem      = mean(relativeNumberOfCustomersReorderedItem), 
    meanMeanNumberOfRepurchases                     = mean(meanNumberOfRepurchases),
    varItemPrice                                    = var(ItemPrice), 
    varRelativeNumberOfRepurchases                  = var(relativeNumberOfRepurchases),
    varRelativeNumberOfCustomersReorderedItem       = var(relativeNumberOfCustomersReorderedItem), 
    maxItemPriceDiff                                = max(ItemPrice) - min(ItemPrice),
    totalNumberOfItems                              = sum(ItemQuantity), 
    totalNumberOfRecords                            = .N
), by = "CustomerID"]


if (dataset == "UKRetail" | "CustomerCountry" %in% colnames(customerOrderDT)) {

    customerFeaturesFromCustLevelDT_subSet <- customerOrderDT_subSet[, list(
        CustomerCountry = min(CustomerCountry)
    ), by = "CustomerID"]

    customerFeaturesDT_subSet <- merge(customerFeaturesFromOrderLevelDT_subSet, customerFeaturesFromItemLevelDT_subSet, 
                                            by = "CustomerID", all.x = TRUE, all.y = TRUE)

}

customerFeaturesDT_subSet <- merge(customerFeaturesDT_subSet, customerFeaturesFromCustLevelDT_subSet, 
                                        by = "CustomerID", all.x = TRUE, all.y = TRUE)

setkey(customerFeaturesDT_subSet, "CustomerID")

customerFeaturesDT_subSet <- customerFeaturesDT_subSet[, RowID := 1:.N]


customerSubcategoryStatsDT_subSet <- customerOrderDT_subSet[, list(
    numberOfOrdersWithSubcategory   = length(unique(OrderID))
), by = c("CustomerID", "ItemSubcategory")]

customerSubcategoryStatsDT_subSet <- customerSubcategoryStatsDT_subSet[!is.na(customerSubcategoryStatsDT_subSet$ItemSubcategory)]

setkey(customerSubcategoryStatsDT_subSet, "CustomerID")

customerSubcategoryStatsDT_subSet <- merge(customerSubcategoryStatsDT_subSet, customerFeaturesDT_subSet[, list(CustomerID, RowID)], 
                                                by = "CustomerID", all.x = TRUE, all.y = FALSE)


subcategoryNumberOfOrdersSparseMatrix_subSet <- sparseMatrix(
    i       = customerSubcategoryStatsDT_subSet$RowID, 
    j       = customerSubcategoryStatsDT_subSet$ItemSubcategory, 
    x       = customerSubcategoryStatsDT_subSet$numberOfOrdersWithSubcategory, 
    dims    = c(max(customerFeaturesDT_subSet$RowID) , max(customerOrderDT$ItemSubcategory))
)

colnames(subcategoryNumberOfOrdersSparseMatrix_subSet) <- paste0("subcategoryNumberOfOrderIndicator_", 1:ncol(subcategoryNumberOfOrdersSparseMatrix_subSet))


customerFeaturesDT_subSet_full <- copy(customerFeaturesDT_subSet)

subcategoryNumberOfOrdersSparseMatrix_subSet_full <- copy(subcategoryNumberOfOrdersSparseMatrix_subSet)


customerTargetDT_subSet <- merge(customerFeaturesDT_subSet[, list(RowID, CustomerID)], customerTargetDT_subSet, 
                                    by = "CustomerID", all.x = TRUE, all.y = FALSE, sort = FALSE)

customerTargetDT_subSet <- customerTargetDT_subSet[is.na(CustomerTotalProfit), CustomerTotalProfit := 0]


customerTargetAllRemainingDT_subSet <- merge(customerFeaturesDT_subSet[, list(RowID, CustomerID)], customerTargetAllRemainingDT_subSet, 
                                                by = "CustomerID", all.x = TRUE, all.y = FALSE, sort = FALSE)

customerTargetAllRemainingDT_subSet <- customerTargetAllRemainingDT_subSet[is.na(CustomerTotalProfit), CustomerTotalProfit := 0]


if (grepl("trainShift", subSet)) {

    eval(parse(text = paste0("customerFeaturesDT_subSet <- customerFeaturesDT_subSet[, (timeVarName) := maxTimeVar_subSet]")))

    eval(parse(text = paste0("customerTargetDT_subSet <- customerTargetDT_subSet[, (timeVarName) := maxTimeVar_subSet]")))

}


if (grepl("dataShift", subSet)) {

    customerFeaturesDT_subSet <- customerFeaturesDT_subSet[, (timeVarName) := maxTimeVar_subSet]

    customerTargetDT_subSet <- customerTargetDT_subSet[, (timeVarName) := maxTimeVar_subSet]

    if (grepl("dataShiftAll", subSet)) {

        customerTargetAllRemainingDT_subSet <- customerTargetAllRemainingDT_subSet[, (timeVarName) := maxTimeVar_subSet]

    }

    if (shiftVal == 0) {

        customerFeaturesDT_all <- customerFeaturesDT_subSet

        customerTargetDT_all <- customerTargetDT_subSet

        subcategoryNumberOfOrdersSparseMatrix_all <- subcategoryNumberOfOrdersSparseMatrix_subSet

        if (grepl("dataShiftAll", subSet)) {

            customerTargetAllRemainingDT_all <- customerTargetAllRemainingDT_subSet

        }

    } else {

        customerFeaturesDT_all <- rbind(customerFeaturesDT_subSet, customerFeaturesDT_all)
        
        customerTargetDT_all <- rbind(customerTargetDT_subSet, customerTargetDT_all)
        
        subcategoryNumberOfOrdersSparseMatrix_all <- rbind(subcategoryNumberOfOrdersSparseMatrix_subSet, subcategoryNumberOfOrdersSparseMatrix_all)
        
        if (grepl("dataShiftAll", subSet)) {
        
            customerTargetAllRemainingDT_all <- rbind(customerTargetAllRemainingDT_subSet, customerTargetAllRemainingDT_all)
        
        }

    }

    if (grepl("dataShiftAll", subSet)) {

        if (shiftVal == max(customerOrderDT[[timeVarName]])) {
                
            if (exists("useLags")) {
            if (useLags) {
                        
                maxLag <- 7        
                for (lagInd in 1:maxLag) {
                
                        customerTotalProfitLagDT <- customerFeaturesDT_all[, c("CustomerID", timeVarName, "totalProfit"), with = FALSE]
                        
                        customerTotalProfitLagDT <- customerTotalProfitLagDT[, (timeVarName) := get(timeVarName) + lagInd]
                        
                        setnames(customerTotalProfitLagDT, "totalProfit", paste0("totalProfitLag_", lagInd))
                        
                        customerFeaturesDT_all <- merge(customerFeaturesDT_all, customerTotalProfitLagDT, 
                                                            by = c("CustomerID", timeVarName), all.x = TRUE, all.y = FALSE, sort = FALSE)
                        
                        customerFeaturesDT_all[[paste0("totalProfitLag_", lagInd)]] <- 
                                    ifelse(is.na(customerFeaturesDT_all[[paste0("totalProfitLag_", lagInd)]]), 
                                                                                                0, customerFeaturesDT_all[[paste0("totalProfitLag_", lagInd)]])

                }
                        
            }
            }
                

            try(saveRDS(customerFeaturesDT_all, file = paste0(path, "customerFeaturesDT_all_", dataset, "_", timeVarName, ".rds")))
            
            try(saveRDS(customerTargetDT_all, file = paste0(path, "customerTargetDT_all_", dataset, "_", timeVarName, ".rds")))
            
            try(saveRDS(subcategoryNumberOfOrdersSparseMatrix_all, file = paste0(path, "subcategoryNumberOfOrdersSparseMatrix_all_", dataset, "_", 
                                                                                        timeVarName, ".rds")))
            
            try(saveRDS(customerTargetAllRemainingDT_all, file = paste0(path, "customerTargetAllRemainingDT_all_", dataset, "_", timeVarName, ".rds")))
            
            gc()

        }

    }

}


if (!grepl("dataShift", subSet)){

    eval(parse(text = paste0("customerFeaturesDT_", subSet, " <- customerFeaturesDT_subSet")))
    
    eval(parse(text = paste0("try(saveRDS(customerFeaturesDT_", subSet, ", file = paste0(path,\"customerFeaturesDT_\",\"", dataset, "_", subSet, ".rds\")))")))
    
    rm(customerFeaturesDT_subSet)
    
    gc()
    
    eval(parse(text = paste0("customerTargetDT_", subSet, " <- customerTargetDT_subSet")))
    
    eval(parse(text = paste0("try(saveRDS(customerTargetDT_", subSet, ", file = paste0(path,\"customerTargetDT_\",\"", dataset, "_", subSet, ".rds\")))")))
    
    rm(customerTargetDT_subSet)
    
    gc()
    
    eval(parse(text = paste0("subcategoryNumberOfOrdersSparseMatrix_", subSet, " <- subcategoryNumberOfOrdersSparseMatrix_subSet")))
    
    eval(parse(text = paste0("try(saveRDS(subcategoryNumberOfOrdersSparseMatrix_", subSet, ", 
                                            file = paste0(path,\"subcategoryNumberOfOrdersSparseMatrix_\",\"", dataset, "_", subSet, ".rds\")))")))
    
    rm(subcategoryNumberOfOrdersSparseMatrix_subSet)
    
    gc()

}


# ------------------------------------------------
# December 2019
# josef.b.bauer (at) gmail.com

