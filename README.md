# CLV_Prediction

Code for the methods proposed in the paper "Customer Lifetime Value Prediction with Sequence-To-Sequence Deep Learning" by Josef Bauer and Dietmar Jannach (University of Klagenfurt, Austria).

It is a framework for predicting the Customer Lifetime Value by combining customized Encoder-Decoder Recurrent Neural Networks and Gradient Boosting Machines. A large number of features is provided which are particularly useful in an e-commerce setting.

The installation of R and several libraries is required, including Keras and TensorFlow. An Anaconda Python distribution should also be installed for a convenient setup.

A detailed description of the individual programs is provided in the file clvRunExperiments.r, which serves as a template of how to execute the individual parts of the process. This includes the installation commands from within R in the file clvLoadLibraries.r.


------------------------------------------------
December 2019

josef.b.bauer (at) gmail.com
